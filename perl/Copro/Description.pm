package Copro::Description;
use Moose;
use namespace::sweep;
use Path::Class::File;
use Data::Table;

has 'desc_dir' => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
    init_arg => "desc-dir",
    );

for my $h ('keys', 'coprop', 'tantiemes', 'lots') {
    has "date_$h" => (
	is       => 'ro',
	isa      => 'Str',
	lazy     => 1,
	required => 1,
	default  => sub {
	    return shift->date_default;
	},
	init_arg => "$h-date",
	);

    has "date_$h".'_real' => (
	is       => 'ro',
	isa      => 'Str',
	lazy     => 1,
	required => 1,
	default  => sub {
	    my $self=shift;
	    my $date="date_$h";
	    return $self->subdir($self->$date);
	},
	init_arg => undef,
	);

    has "$h" => (
	is       => 'ro',
	isa      => 'Data::Table',
	lazy     => 1,
	required => 1,
	default  => sub {
	    return shift->_loadData($h);
	},
	init_arg => undef,
	);

}

has "lotstantiemes" => (
    is       => 'ro',
    isa      => 'Data::Table',
    lazy     => 1,
    default  => sub {
	my $self = shift;
	return $self->lots->join($self->tantiemes, Data::Table::INNER_JOIN,
			      [ "Lot" ], ["Lot"]);
    },
    init_arg => undef,
    );

has "lotstantiemescoprop" => (
    is       => 'ro',
    isa      => 'Data::Table',
    lazy     => 1,
    default  => sub {
	my $self = shift;
	return $self->lots->join(
	    $self->tantiemes, Data::Table::INNER_JOIN,
	    [ "Lot" ], ["Lot"])->join(
	    $self->coprop, Data::Table::INNER_JOIN,
	    [ "Coprop" ], [ "Coprop" ]);
    },
    init_arg => undef,
    );

has "date_default" => (
    is       => 'ro',
    isa      => 'Maybe[Str]',
    required => 0,
    init_arg => "default-date",
    );

# cache
my $subdirs;
my $tables;

sub subdir {
    my $self = shift;
    my $date = shift;
    
    if (!exists($subdirs->{$self->desc_dir}->{$date})) {
	my $subdir;
	my $dir=$self->desc_dir;
	if ($date =~ /^[0-9]{4}$/) {
	    $subdir=$date."-end";
	} elsif ($date =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/) {
	    opendir DIR, $dir or die "cannot open dir $dir: $!";
	    my @files= readdir DIR;
	    closedir DIR;
	    foreach my $file (sort @files) {
		next if ($file !~ /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/);
		last if ($file gt $date);
		$subdir=$file;
	    }
	} else {
	    die "E: Invalid date format '$date' when loading copro\n";
	}
	$subdirs->{$self->desc_dir}->{$date}=$subdir;
    }
    return $subdirs->{$self->desc_dir}->{$date};
}

my $filename = {
    'coprop' => 'coproprietaires',
};

sub _loadData {
    my $self = shift;
    my $hname=shift;
    my $name=$filename->{$hname} // $hname;
    my $date="date_$hname";
    my $subdir = $self->subdir($self->$date);

    if (!exists($tables->{$self->desc_dir}->{$subdir}->{$name})) {
	my $file=Path::Class::File->new($self->desc_dir, $subdir, $name.".csv");
	my $fh=$file->openr() or die "cannot open $file: $!";
	binmode( $fh, ":utf8");
	
	my $data = Data::Table::fromCSV($fh, 1, undef,
					{delimiter=>','});
	$tables->{$self->desc_dir}->{$subdir}->{$name}=$data;
	$fh->close();
	return $data;
    }
    return $tables->{$self->desc_dir}->{$subdir}->{$name};
};

sub infotable {
    my $self=shift;
    my $table=shift;
    my $id=shift;
    my $value=shift;

    my $search = "\$_{'$id'} eq '".$value."'";
    return $table->match_pattern_hash($search);
}

sub info {
    my $self=shift;
    my $part=shift;

    return $self->infotable($self->$part, @_);
}

sub CopropFromAccount {
    my $self = shift;
    my $account = shift;
    my $info=$self->info('coprop', 'Account', $account);
    return $info->elm(0, 'Coprop');
}

sub CopropPart {
    my $self = shift;
    my $coprop = shift;
    my $key = shift;

    my $info=$self->info('lotstantiemes', 'Coprop', $coprop);
    my $info2=$self->infotable($info, 'K.Id', $key);
    my $tantiemes=$info2->col('tantiemes');
}
1;
