package Copro::Types;

use Moose::Util::TypeConstraints;
use MooseX::Getopt;

use Math::BigRat;
class_type 'Math::BigRat';
coerce 'Math::BigRat',
    from 'Num',
    via { Math::BigRat->new($_) };
MooseX::Getopt::OptionTypeMap->add_option_type_to_map(
    'Math::BigRat' => '=f'
    );

use Path::Class::File;
class_type 'Path::Class::File';
#coerce 'Path::Class::File',
#    from 'Str',
#    via { Path::Class::File->new($_) };
MooseX::Getopt::OptionTypeMap->add_option_type_to_map(
    'Path::Class::File' => '=s'
    );


1;
