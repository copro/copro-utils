package Copro::Utils::Ledger;
use strict;
use warnings;
use List::Util qw(min);
use Scalar::Util qw(blessed refaddr);
use Math::BigRat;
use utf8;
use Carp;

my %dump_journals_default_options = (
    'select-transaction' => sub {
	return 1;
    },
    'print-transactions-only' => 0,
    'merge-transactions' => 0,
    'edit-transaction' => sub {
	my $self=shift;
	return $self;
    },
    'print' => sub {
	my $str = shift;
	print $str;
    },
    );

sub dump_journal {
    my $journal = shift;
    my %options = (%dump_journals_default_options, @_);
    
    foreach my $option (keys %options) {
	if (!exists($dump_journals_default_options{$option})) {
	    carp "Unkown option $option";
	}
    }

    my $iter=$journal->iterator(
	'follow-includes'=>1,
	'filter-out-element' => sub {
	    my $e = shift;
	    if ($e->isa('Ledger::Transaction')) {
		# Transaction
		#if ($options{'select-transaction'}) {
		    if (!$options{'select-transaction'}->($e)) {
			return 0;
		    }
		#}
		return 2;
	    } else {
		return 0;
	    }
	},
	);
    while (defined(my $e=$iter->next)) {
	if ($e->isa('Ledger::Transaction')) {
	    $e=$options{'edit-transaction'}->($e);
	    next unless defined($e);
	    $options{'print'}->($e->as_string);
	} else {
	    # Non transaction
	    if ($options{'print-transactions-only'} // 0) {
		next;
	    }
	    $options{'print'}->($e->gettext);
	}
    }
}

sub set_amount {
    my $p = shift;

    if (! $p->isa("Ledger::Posting")) {
	croak "Invalid object $p, not a Ledger::Posting";
    }
    return if ($p->has_var('amount'));
    my $amount=$p->amount->amount;
    if (! defined($amount)) {
	$amount=Math::BigRat->new(0);
	my $iter=$p->parent->iterator(
	    'select-element' => sub {
		return shift->isa("Ledger::Posting");
	    });
	while (defined(my $post=$iter->next)) {
	    next if refaddr($post) == refaddr($p);
	    $amount -= $post->amount->amount;
	}
    }
    $p->var('amount', $amount);
}

sub set_rcdate {
    my $t = shift;
    if (! $t->isa("Ledger::Transaction")) {
	croak "Invalid object $t, not a Ledger::Transaction";
    }
    if (! $t->has_var("rcdate")) {
	$t->var("rcdate", $t->auxdate // $t->date);
	$t->var("rcdate_str",  $t->var("rcdate")->strftime('%Y-%m-%d'));
    }
}

sub set_knowndate {
    my $t = shift;
    if (! $t->isa("Ledger::Transaction")) {
	croak "Invalid object $t, not a Ledger::Transaction";
    }
    set_rcdate($t);
    if (! $t->has_var("knowndate")) {
	my $tag;

	$tag=$t->tag('PIECE');
	if (defined($tag) 
	    && $tag =~ m,^Archives://[0-9]+/AG/([0-9]{4}-[01]-[0123][0-9])/,) {
	    $t->var("knowndate_str", $1);
	    return;
	} 

	$tag=$t->tag('APPEL-DATE');
	if (defined($tag)) {
	    $t->var("knowndate_str", $tag);
	    return;
	}

	$t->var("knowndate_str", $t->var("rcdate_str"));
    }
}

sub journal_date_minmax {
    my $journal = shift;
    
    my $iter=$journal->iterator(
	'follow-includes'=>1,
	'select-element' => sub {
            return shift->isa("Ledger::Transaction");
        });

    my ($dstart, $dend);
    while (defined(my $t=$iter->next)) {
	my $d=$t->date_str;
	if (not defined($dstart)) {
	    $dstart = $dend = $d;
	} else {
	    if ($d lt $dstart) {
		$dstart=$d;
	    } elsif ($d gt $dend) {
		$dend=$d;
	    }
	}
    }
    return ($dstart,$dend);
}

use Exporter qw(import);
our @EXPORT_OK = qw(dump_journal set_amount set_rcdate set_knowndate journal_date_minmax);
#my @list = qw(amount_round);
#our @EXPORT_OK = (@list, qw(formatAccount));
#our %EXPORT_TAGS = ( 'constants' => \@list, 'all' => \@EXPORT_OK );

1;
