package Copro::Utils::Config::Section;
use Moose;
use namespace::autoclean;

sub compta {
    my $self=shift;
    return $self->baseconfig->compta;
}

has 'section' => (
    is => 'ro',
    isa => 'Str',
    required => 1,
    weak_ref => 1,
    );

has 'baseconfig' => (
    is => 'ro',
    isa => 'Copro::Utils::Config',
    required => 1,
    weak_ref => 1,
    );

has 'values' => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef',
    default   => sub { {} },
    handles   => {
	set      => 'set',
	get      => 'get',
	isset    => 'exists',
	#count          => 'count',
	#option_pairs   => 'kv',
    },
    );

sub push {
    my $self = shift;
    my $name = shift;

    if ($self->isset($name)) {
	push @{$self->get($name)}, @_;
    } else {
	$self->set($name, [ @_ ]);
    }
    #print "EX: '".join(', ', @{$self->get($name)})."'\n";
}

1;
