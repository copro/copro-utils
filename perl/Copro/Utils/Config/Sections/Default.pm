package Copro::Utils::Config::Sections::Default;
use Moose;
use namespace::autoclean;

extends 'Copro::Utils::Config::Section';

sub exercices {
    my $self=shift;

    my $exercices=$self->get("exercices");
    return map {
	if ($_ eq 'all') {
	    $self->compta->exercices->all;
	} else {
	    $self->compta->exercice($_)
	}
    } @{$exercices};
}

#use Carp;
#before 'set' => sub {
#    my $self = shift;
#    my $name = shift;
#    my $value= shift;
#
#    if (not defined($value)) {
#	confess "value for $name not defined";
#    }
#    print "Setting  $name to $value\n";
#    
#};

before 'get' => sub {
    my $self = shift;
    my $name = shift;

    #print "Getting $name\n";
    if (not $self->isset($name)) {
	if ($name eq 'exercices') {
	    use Time::Piece;
	    my $t = Time::Piece->new();
	    #print "default value ".$t->year."\n";
	    $self->set($name, [ $t->year ] );
	}
    }
};


1;
