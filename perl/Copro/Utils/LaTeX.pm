package Copro::Utils::LaTeX;
use strict;
use warnings;
use utf8;
use Carp;

use List::AllUtils qw'natatime';

sub latexize {
    my $s = shift;
    $s =~ s/([0-9])%/$1\\,\\%/g;
    $s =~ s/_/\\_/g;
    return $s;
}

sub hash2options {
    my @options;
    
    my $iter = natatime 2, @_;
    while( my($name,$val) = $iter->() ){
	push @options, "$name={$val}";
    }
    return join(',', @options);
}
    
use Exporter qw(import);
our @EXPORT_OK = qw(latexize hash2options);
#my @list = qw(amount_round);
#our @EXPORT_OK = (@list, qw(formatAccount));
#our %EXPORT_TAGS = ( 'constants' => \@list, 'all' => \@EXPORT_OK );

1;
