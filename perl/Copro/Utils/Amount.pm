package Copro::Utils::Amount;

use Math::BigRat;
use utf8;

sub amount_round {
    my $amount=shift;
    my $factor = shift // Math::BigRat->new(1);
    $amount=Math::BigRat->new($amount)*$factor;
    if ($amount->is_neg) {
	return Math::BigRat->new(
	    (-$amount*100 + 0.5)->as_int)
	    /-100;
    } else {
	return Math::BigRat->new(
	    ($amount*100 + 0.5)->as_int)
	    /100;
    }
}

sub amount_str {
    my $value=shift;
    my $currency=shift || '€';
    if ($value->is_int) {
	return sprintf("%6.0f    %s", $value, $currency);
    } else {
	return sprintf("%9.2f %s", $value, $currency);
    }
}

sub val_str {
    my $value=shift;
    my $precision=shift // 2;
    if ($value->is_int) {
	return sprintf("%.0f", $value);
    } else {
	return sprintf("%.$precision"."f", $value);
    }
}

use Exporter qw(import);
our @EXPORT_OK = qw(amount_round amount_str val_str);
#my @list = qw(amount_round);
#our @EXPORT_OK = (@list, qw(formatAccount));
#our %EXPORT_TAGS = ( 'constants' => \@list, 'all' => \@EXPORT_OK );

1;
