package Copro::Utils::Roles::Date;
use Moose::Role;
use namespace::sweep;


sub today() {
	my @today=localtime();

	return sprintf("%4i-%02i-%02i", $today[5]+1900, $today[4]+1, $today[3]);
}

1;
