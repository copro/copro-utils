package Copro::Utils::Config;
use Moose;
use namespace::autoclean;

use Copro::Utils::Config::Section;
    
with qw(Copro::Compta::Roles::ComptaAttr);

has 'sections' => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef[Copro::Utils::Config::Section]',
    default   => sub { {} },
    handles   => {
	_register_section    => 'set',
	section              => 'get',
	has_section          => 'exists',
	#count          => 'count',
	#option_pairs   => 'kv',
    },
    );

require UNIVERSAL::require;
before section => sub {
    my $self = shift;
    my $secname = shift;
    
    if (not $self->has_section($secname)) {
	my $pkgname=$secname;
	$pkgname =~ s/(\w+)/\u$1/g;
	$pkgname =~ s/-/::/g;
	my $secPKG="Copro::Utils::Config::Sections::$pkgname";
	if (not $secPKG->require) {
	    #print "Cannot find $secPKG, using default section\n$@";
	    $secPKG="Copro::Utils::Config::Section";
	}
	my $secOBJ=$secPKG->new(
	    'section' => $secname,
	    'baseconfig' => $self,
	    );
	
	$self->_register_section($secname, $secOBJ);
    }
    #return $self->cmds->get($cmd);
};

sub default {
    my $self = shift;
    return $self->section('default')
}

sub internal {
    my $self = shift;
    return $self->section('internal')
}


__PACKAGE__->meta->make_immutable;

1;
