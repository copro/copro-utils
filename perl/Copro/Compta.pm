package Copro::Compta;
use Moose;
use namespace::autoclean;

use MooseX::Types::Path::Class;
use Copro::Compta::Exercices;
use Copro::Compta::Exercice;
use Copro::Compta::TropexCol;
use Copro::Compta::Tropex;

use Text::CSV;

package Copro::Compta::CMDS;
use Module::Pluggable
    sub_name => 'loadcmds',
    search_path => 'Copro::Compta::CMD',
    instantiate => 'new';
sub new {
      my $class = shift;
      return bless {}, $class;
}
1;
package Copro::Compta;
#require 'Copro::Compta::CMDS';

has 'path' => (
    is => 'ro',
    isa => 'Path::Class::Dir',
    writer => '_set_path',
    required => 1,
    coerce   => 1,
    );

has 'mainfile' => (
    is => 'ro',
    isa      => 'Path::Class::File',
    writer   => '_set_mainfile',
    required => 1,
    coerce   => 1,
    default  => sub { "copro-compta.csv"; },
    );

has 'exercices' => (
    is        => 'ro',
    isa       => 'Copro::Compta::Exercices',
    default   => sub { Copro::Compta::Exercices->new() },
    handles   => {
	registerExercice => 'register',
	#get_option     => 'get',
	exercice       => 'exercice',
	countExercices  => 'count',
	#option_pairs   => 'kv',
    },
    );

has 'tropexfile' => (
    is => 'ro',
    isa      => 'Path::Class::File',
    writer   => '_set_tropexfile',
    required => 1,
    coerce   => 1,
    default  => sub { "tropex.csv"; },
    );

has 'tropexes' => (
    is        => 'ro',
    isa       => 'Copro::Compta::TropexCol',
    default   => sub { Copro::Compta::TropexCol->new() },
    handles   => {
	registerTropex  => 'register',
	#get_option     => 'get',
	tropex         => 'tropex',
	allTropex      => 'all',
	countTropex    => 'count',
	#option_pairs   => 'kv',
    },
    );

#use Data::Dumper;
use Copro::Compta::Roles::CMD;
has 'cmds' => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef[Copro::Compta::Roles::CMD]',
    #isa       => 'HashRef[Copro::Compta::CMD::Annexes]',
    #isa       => 'HashRef',
    #default   => sub { {} },
    lazy      => 1,
    default   => sub {
	my $self=shift;
	my $cmds=Copro::Compta::CMDS->new();
	my @cmds=$cmds->loadcmds('compta' => $self);
	my $h={};
	foreach my $cmd (@cmds) {
	    my $name = lc($cmd->cmdname);
	    #print STDERR "Registering ", $cmd->cmdname, "\n";
	    $h->{$name} = $cmd;
	}
	#print STDERR Dumper($h), "\n";
	return $h;
    },
    handles   => {
	#_register_cmd  => 'set',
	has_cmd        => 'exists',
	cmd            => 'get',
	cmdnames       => 'keys',
    },
    );

#sub has_cmd {
#    my $self = shift;
#    my $cmdname = shift;
#    return exists($self->cmds->{$cmdname});
#}
    
before cmd => sub {
#sub cmd {
    my $self = shift;
    my $cmd = shift;
     
    if (not $self->has_cmd($cmd)) {
	die "Unable to load command '$cmd': $@";
    }
};

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;
    
    if ( @_ == 1 && !ref $_[0] ) {
	return $class->$orig( path => $_[0] );
    }
    else {
	return $class->$orig(@_);
    }
};

# require UNIVERSAL::require;
# before cmd => sub {
# #sub cmd {
#     my $self = shift;
#     my $cmd = shift;
    
#     if (not $self->has_cmd($cmd)) {
# 	my $pkgname=$cmd;
# 	$pkgname =~ s/(\w+)/\u$1/g;
# 	$pkgname =~ s/-/::/g;
# 	my $cmdPKG="Copro::Compta::CMD::$pkgname";
# 	if (!$cmdPKG->require) {
# 	    my $filename=$cmdPKG.'.pm';
# 	    $filename =~ s,::,/,g;
# 	    if ($@ =~ /^Can't locate $filename in /) {
# 		die "Unknown command '$cmd'. Aborting.\n";
# 	    } else {
# 		die "Unable to load command '$cmd': $@";
# 	    }
# 	}
# 	my $cmdOBJ=$cmdPKG->new(
# 	    'compta' => $self,
# 	    );
# 	if (not $cmdOBJ->meta->does_role('Copro::Compta::Roles::CMD')) {
# 	    die "Invalid loaded class $cmdPKG\n".
# 		"It does not support the Copro::Compta::Roles::CMD Role\n";
# 	}
	
# 	$self->_register_cmd($cmd, $cmdOBJ);
#     }
#     #return $self->cmds->get($cmd);
# };

sub execCMD {
    my $self = shift;
    my $cmd = shift;

    $self->cmd($cmd)->run(@_);
}

sub help {
    my $self = shift;

    my $res;
    foreach my $cmd (sort $self->cmdnames) {
	$res .= sprintf("  %- 15s : ", "$cmd");
	$res .= $self->cmd($cmd)->synopsis."\n";
    }
    return $res;
}

sub BUILD {
    my $self = shift;

    if ($self->mainfile->is_relative) {
	$self->_set_mainfile(Path::Class::File->new($self->path, $self->mainfile));
    }
    if ($self->mainfile->is_relative) {
	$self->_set_mainfile($self->mainfile->absolute);
    }
    $self->_load_mainfile;
    if ($self->tropexfile->is_relative) {
	$self->_set_tropexfile(Path::Class::File->new($self->path, $self->tropexfile));
    }
    if ($self->tropexfile->is_relative) {
	$self->_set_tropexfile($self->tropexfile->absolute);
    }
    $self->_load_tropex;

    
    return;
}

sub _load_mainfile {
    my $self=shift;

    my $csv = Text::CSV->new ({
	binary    => 1,
	auto_diag => 1,
	sep_char  => ';',
			      });
    my $fh=$self->mainfile->open('<:encoding(utf8)')
	or die "Could not open '".$self->mainfile."': $!\n";

    {
	my $headers=$csv->getline($fh);
	my %headers=();
	$headers{$_}++ for (@{$headers});
	$csv->column_names(@{$headers});
	foreach my $reqcol ('id', 'type', 'subtype', 'path', 'pid', 'label') {
	    if (not exists($headers{$reqcol})) {
		die "Missing column '".$reqcol."' in file '".
		    $self->mainfile."'\n";
	    }
	}
    }

    my $config={};
    my $prev_exercice=undef;
    while(my $hline=$csv->getline_hr($fh)) {
	next if $hline->{'id'} eq '';
	next if $hline->{'id'} =~ /^#/;
	if ($hline->{'id'} eq 'ROOT') {
	    if ($hline->{'pid'} ne '') {
		die "Invalid line in '".$self->mainfile."'\n".
		    "ROOT cannot have a pid\n";
	    }
	    if ($hline->{type} ne 'DIR') {
		die "Invalid line in '".$self->mainfile."'\n".
		    "ROOT must be a directory\n";
	    }
	    if ($hline->{subtype} ne '') {
		die "Invalid line in '".$self->mainfile."'\n".
		    "ROOT cannot have a subtype\n";
	    }
	    my $dir=Path::Class::Dir->new($hline->{path});
	    if (!$dir->is_absolute) {
		$dir=Path::Class::Dir->new($self->path, $dir);
	    }
	    $self->_set_path($dir->absolute);
	    $config->{$hline->{'id'}}=$self;
	} else {
	    my $pid=$hline->{'pid'};
	    if (not exists($config->{$pid})) {
		die "Invalid line in '".$self->mainfile."'\n".
		    "Unknown pid '".$pid."'\n";
	    }
	    my $type=$hline->{'type'};
	    my $id=$hline->{'id'};
	    if ($type eq 'FinancialYear') {
		my $exercice=Copro::Compta::Exercice->new(
		    %{$hline},
		    'compta' => $self,
		    'prev' => $prev_exercice,
		    );
		$self->registerExercice($id, $exercice);
		$config->{$hline->{'id'}}=$exercice;
		$prev_exercice = $exercice;
	    } elsif ($type eq 'Ledger') {
		my $exercice=$config->{$hline->{'pid'}};
		if (not $exercice->isa('Copro::Compta::Exercice')) {
		    die "Invalid line in '".$self->mainfile."'\n".
		    "'".$id."' must have a FinancialYear as pid\n";
		}
		my $ledger=$exercice->addledger(%{$hline});
		$config->{$hline->{'id'}}=$ledger;
	    } else {
		die "Invalid line in '".$self->mainfile."'\n".
		    "Unknown type '".$type."'\n";
	    }
	}
	    
    }
    if (not $csv->eof) {
	$csv->error_diag();
    }
    close $fh;
}

sub _load_tropex {
    my $self=shift;

    my $csv = Text::CSV->new ({
	binary    => 1,
	auto_diag => 1,
	sep_char  => ';',
			      });
    my $fh=$self->tropexfile->open('<:encoding(utf8)')
	or die "Could not open '".$self->tropexfile."': $!\n";

    {
	my $headers=$csv->getline($fh);
	my %headers=();
	$headers{$_}++ for (@{$headers});
	$csv->column_names(@{$headers});
	foreach my $reqcol ('tag', 'type', 'start', 'end', 'label') {
	    if (not exists($headers{$reqcol})) {
		die "Missing column '".$reqcol."' in file '".
		    $self->tropexfile."'\n";
	    }
	}
    }

    my $config={};
    while(my $hline=$csv->getline_hr($fh)) {
	next if $hline->{'tag'} eq '';
	$self->registerTropex(
	    $hline->{'tag'},
	    Copro::Compta::Tropex->new(
		'compta' => $self,
		%{$hline},
	    ));
    }
    if (not $csv->eof) {
	$csv->error_diag();
    }
    close $fh;
}

sub select_ledgers {
    my $self=shift;
    my $config=shift;
    my %options = (
	'kind' => [],
	'load' => 0,
	'exercices' => undef,
	@_,
	);
    my $kind=shift // '';
    #my $kind=$expr->{'kind'};
    
    my @ledgers=();
    my @exercices;
    if (defined($options{'exercices'})) {
	@exercices=@{$options{'exercices'}};
    } else {
	@exercices=$config->default->exercices;
    }
	
    foreach my $exercice (@exercices) {
	if (not defined($exercice)) {
	    die "Unknown exercice in '".join("', '", @{$config->default->get("exercices")})."'\n";
	}
	if ($options{'load'}) {
	    $exercice->load;
	}
      LEDGER:
	foreach my $l ($exercice->ledgers) {
	    my $ok=1;
	    foreach my $kind (@{$options{'kind'}}) {
		if (not $l->iskind($kind)) {
		    next LEDGER;
		}
	    }
	    push @ledgers, $l;
	}
    }
    return @ledgers;
}

__PACKAGE__->meta->make_immutable;

1;
