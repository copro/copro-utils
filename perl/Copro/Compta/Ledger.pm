package Copro::Compta::Ledger;
use Moose;
use namespace::autoclean;
use Path::Class::Dir;
use Ledger::Parser;
use MooseX::ClassAttribute;

sub compta {
    my $self=shift;
    return $self->exercice->compta;
}

has 'exercice' => (
    is => 'ro',
    isa => 'Copro::Compta::Exercice',
    required => 1,
    weak_ref => 1,
    );

has 'label' => (
    is  => 'ro',
    isa => 'Str',
    required => 1,
    builder => '_build_label',
    lazy => 1,
    );

sub _build_label {
    my $self=shift;
    return $self->id." journal";
}

has 'id' => (
    is  => 'ro',
    isa => 'Str',
    required => 1,
    );

has 'path' => (
    is  => 'ro',
    isa => 'Path::Class::File',
    lazy => 1,
    required => 1,
    builder => '_build_path',
    writer => '_set_path',
    init_arg => undef,
    );

has 'relpath' => (
    is  => 'ro',
    isa => 'Path::Class::File',
    lazy => 1,
    coerce   => 1,
    required => 1,
    builder => '_build_relpath',
    writer => '_set_relpath',
    init_arg => 'path',
    trigger => \&_relpath_set,
    );

sub _build_path {
    my $self = shift;
    my $path=Path::Class::File->new($self->exercice->path, $self->relpath);
    return $path;
}

sub _build_relpath {
    my $self = shift;
    return $self->id.".legder";
}

sub _relpath_set {
    my ( $self, $relpath, $old_relpath ) = @_;

    my $path=Path::Class::File->new($relpath);
    if (not $path->is_relative) {
	die "Setting path with a non relative path '".$path.
	    "' in Ledger '".$self->id."'\n";
    }
    $self->_set_path(Path::Class::File->new($self->exercice->path, $path))
}

has "kind" => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef[Str]',
    required  => 1,
    handles   => {
	#set      => 'set',
	#get      => 'get',
	iskind   => 'exists',
	#count          => 'count',
	#option_pairs   => 'kv',
    },
    );

sub Str2kind {
    my $str = shift;
    my %hash= map { $_ => 1 } (split(',', $str));
    return \%hash;
}

class_has "_parser" => (
    isa      => 'Ledger::Parser',
    is       => 'ro',
    default  => sub { Ledger::Parser->new() },
    lazy     => 1,
    );

has "journal" => (
    isa      => 'Ledger::Journal',
    is       => 'ro',
    default  => sub {
	my $self = shift;
	print STDERR "Reading ".$self->path."\n";
	$self->_parser->read_file($self->path);
    },
    lazy     => 1,
    );

__PACKAGE__->meta->make_immutable;

1;
