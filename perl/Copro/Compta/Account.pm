package Copro::Compta::Account;
use Moose;
use namespace::sweep;
use Carp;

has '_aliases' => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef[Bool]',
    default   => sub { {} },
    handles   => {
	_registerAlias => 'set',
	#_getAlias      => 'get',
	aliases        => 'keys',
	#count          => '_count',
	#option_pairs   => '_kv',
	#_existsAlias   => 'exists',
	#isAlias        => 'exists',
	hasAlias        => 'exists',
    },
    );

has 'name' => (
    isa        => 'Str',
    is         => 'ro',
    required   => 1,
    );

has 'note' => (
    isa        => 'Str',
    is         => 'rw',
    predicate  => 'hasNote',
    clearer    => 'resetNote',
    );

has 'accounts' => (
    isa        => 'Copro::Compta::Accounts',
    is         => 'ro',
    required   => 1,
    weak_ref   => 1,
    );

sub parent {
    my $self = shift;
    my $name = $self->name;
    if ($name !~ /(.*):[^:]+$/) {
	return undef;
    }
    my $pname=$1;
    my $p=$self->accounts->byName($pname);
    if (not defined($p)) {
	$p = $self->new(
	    'accounts' => $self->accounts,
	    'name' => $pname,
	    );
    }
    return $p;
}

sub addAlias {
    my $self = shift;
    my $alias = shift;

    $self->_registerAlias($alias, 1);
    $self->accounts->_registerAliases($self);
}

sub removeAlias {
    my $self = shift;
    my $alias = shift;
    delete($self->_aliases->{$alias});
    $self->accounts->_removeAlias($alias);
}

__PACKAGE__->meta->make_immutable;

1;
