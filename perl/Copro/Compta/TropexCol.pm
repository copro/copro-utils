package Copro::Compta::TropexCol;
use Moose;
use namespace::sweep;

has '_tropexes' => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef[Copro::Compta::Tropex]',
    default   => sub { {} },
    handles   => {
	register       => 'set',
	tropex         => 'get',
	all            => 'values',
	count          => 'count',
	option_pairs   => 'kv',
	exercices      => 'values',
    },
    );

1;
