package Copro::Compta::Balances;
use strict;
use warnings;
use Math::BigRat;
use utf8;
use Carp;
use Copro::Utils::Ledger;

use Readonly;

my %params = (
    'annexe1' => [
	'--limit', 'account=~/^[145]:/ && tag("PHASE") != "CLOTURE"',
	'--group-by',
	'"GROUP: " + (account=~/^5:/ ? "TRESORERIE" : '
	.'(account=~/^[4]:/ ? "CREANCES+DETTES" : '
	.'(account=~/^1:/ ? "AVANCES" : "DETTES")))',
    ],
    'annexe1-next' => [
	'--limit', 'account=~/^4:(7|5:0)/ && tag("PHASE") != "CLOTURE"',
	'--group-by',
	'"GROUP: " + (account=~/^4:7/ ? "ATTENTE" : "COPROP" )',
    ],
    'annexe2' => [
	'--limit', 'account=~/^[67]:/',
	'--group-by',
	'"GROUP: " + ((tag("PHASE") == "CLOTURE" || tag("PHASE") == "REPARTITION" )? "OUT_ANNEXE2" : '
	.'( (account=~/^6/ ? "CH-" : "PR-" )+(tag("REPTYPECH")=="CHCOUR" ? "COUR" : "TROPEX")))',
    ],
    'annexe3' => [
	'--limit', 'account=~/^[6]:/ && tag("REPTYPECH")=="CHCOUR" && '
	.'( (""+tag("PHASE"))=="" || tag("PHASE")=="BUDGET" )',
	'--group-by',
	'"GROUP: " +  (tag("REPRA") ? "RA" : "NET" ) + "/" + tag("REPCLEF")',
    ],
    'annexe4' => [
	'--limit', 'account=~/^[67]:/ && tag("REPTYPECH")!="CHCOUR" && '
	.'( (""+tag("PHASE"))=="" || tag("PHASE")=="OUVERTURE" || tag("PHASE")=="TROPEX_WAIT" || tag("PHASE")=="BUDGET" )',	
	'--group-by',
	'"GROUP: " + (account=~/^6:/ ? "CH" : "PR") + "/" + tag("REPTYPECH") + "/" + tag("REPTROPEX") + "/" '.
	' + (tag("REPRA") ? "RA" : "NET" ) + "/" + tag("REPCLEF")',
    ],
    'annexe5' => [
	'--limit', 'account=~/^[67]:/ && tag("REPTYPECH")!="CHCOUR" && tag("PHASE") == "TROPEX_WAIT"',
	'--group-by',
	'"GROUP: " + ('
	.'(tag("REPTYPECH") + "/" + tag("REPCLEF") + "/" + tag("REPTROPEX")))',
    ],
    );

Readonly my $BALANCE_FORMAT =>
    '%(ansify_if('.
    '    " "+justify(scrub(display_total), 15,'.
    '          50 + int(prepend_width), true, color),'.
    '    bold if should_bold))'.
    '  %-(ansify_if('.
    '       ansify_if('.
    '         justify(account, 30),'.
    '         blue if color),'.
    '       bold if should_bold))'.
    '  %-(ansify_if('.
    '       ansify_if(account.note, blue if color),'.
    '       bold if should_bold))\n%/'.
    '%(ansify_if("TOTAL:  ", bold if should_bold))%$1\n'
    ;

sub getSupportedAnnexes {
    my %bals = ( %params, @_ );
    return sort (keys %bals);
}

sub getParamsForAnnexe {
    my $annexe=shift;
    my %bals = ( %params, @_ );
    return $bals{$annexe};
}

sub run {
    my $config = shift;
    my $compta = shift;
    my %options = (
	'start-args' => [],
	'mid-args' => [],
	'end-args' => [],
	'args' => [],
	'select' => [],
	'ledger-kind' => [ 'journal' ],
	@_,
	);

    my @ledgerargs=(
	'ledger',
	@{$options{'start-args'}},
	'-f', 'structure.ledger',
	'-T', '{1 €}*(O)',
	'--pedantic',
	'--balance-format', $BALANCE_FORMAT,
	@{$options{'mid-args'}},
	'-f', '-',
	@{$options{'end-args'}},
	'bal',
	@{$options{'args'}},
	);


    my $journal_exercice="";
    foreach my $ledger (
	$compta->select_ledgers(
	    $config,
	    'exercices' => $options{'exercices'},
	    'kind' => ['pc'],
	    'load' => 1,
	),
	$compta->select_ledgers(
	    $config,
	    'exercices' => $options{'exercices'},
	    'kind' => $options{'ledger-kind'},
	),
	) {
	$journal_exercice .= "include ".$ledger->path."\n";
    }
    my $journal=Copro::Compta::Ledger->_parser->read_string($journal_exercice);

    #print STDERR "Read with parser ",Copro::Compta::Ledger->_parser,"\n";
    
    my $dumpledger_pid = open(LEDGERDUMPED, "|-");
    if ($dumpledger_pid)
    {
	# am the parent:
	# either write TO_KID or else read FROM_KID
	binmode(STDERR, ":utf8");
	binmode(LEDGERDUMPED, ":utf8");

	Copro::Utils::Ledger::dump_journal(
	    $journal,
	    @{$options{'select'}},
	    'print' => sub {
		my $str = shift;
		print LEDGERDUMPED $str;
	    }
	    );
	close(LEDGERDUMPED);
	waitpid($dumpledger_pid, 0);
	return;
    }
    # am the child; use STDIN/STDOUT normally

    binmode(STDERR, ":utf8");
    print STDERR "RUN: '", join("' '", @ledgerargs), "'\n";
    exec @ledgerargs or die "Cannot execute ledger\n";
}

sub get {
    my $config = shift;
    my $compta = shift;
    my %options = (
	'ensure-balances' => [],
	@_,
	);
    # loading exercices in main processus
    $compta->select_ledgers(
	$config,
	'exercices' => $options{'exercices'},
	'kind' => ['pc'],
	'load' => 1,
	);
    
    my $ledger_pid = open(LEDGEROUTPUT, "-|");

    if ($ledger_pid)
    {
	# am the parent:
	# either write TO_KID or else read FROM_KID
	binmode(STDERR, ":utf8");
	binmode(LEDGEROUTPUT, ":utf8");

	my $group="";
	my $ligne;
	my $balances={};
	my $total = {};
	while (defined($ligne = <LEDGEROUTPUT>)) {
	    chomp($ligne);
	    if ($ligne =~ /^GROUP:\s+(.*\S)\s*$/) {
		$group=$1;
	    } elsif ($ligne =~ /^TOTAL:\s*(-?[0-9]*[.]?[0-9]*)\s*[€]?\s*$/) {
		my $value=Math::BigRat->new($1);
		$balances->{$group}->{total}=$value;
	    } elsif ($ligne =~ /^\s+(-?[0-9]*[.]?[0-9]*)\s*[€]?\s+(\S+([ ]\S+)*)([ ][ ]|\t)/) {
		my $account=$2;
		my $value=Math::BigRat->new($1);
		if (exists($balances->{$group}->{accounts}->{$account})) {
		    carp "Warning: account '$account' already in balance '$group'\n";
		    $balances->{$group}->{accounts}->{$account}+=$value;
		} else {
		    $balances->{$group}->{accounts}->{$account}=$value;
		}
		$total->{$group} += $value;
	    } elsif ($ligne =~ /^\s*$/) {
	    } else {
		carp "Warning: unknown line '$ligne' in balance '$group'\n";
	    }
	}
	close(LEDGEROUTPUT);
	foreach $group (@{$options{'ensure-balances'}}) {
	    if (!exists($balances->{$group})) {
		$balances->{$group}->{accounts}={};
		$balances->{$group}->{total}=Math::BigRat->new(0);
		$total->{$group}=Math::BigRat->new(0);
	    }
	}
	foreach $group (keys(%{$balances})) {
	    if (!exists($balances->{$group}->{total})) {
		if (scalar(keys(%{$balances->{$group}->{accounts}})) != 1) {
		    carp "Warning: no total for balance '$group'\n";
		}
		$balances->{$group}->{total}=$total->{$group};
	    } else {
		if ($balances->{$group}->{total} != $total->{$group}) {
		    carp "Warning: different totals (read ".$balances->{$group}->{total}
		    .", computed ".$total->{$group}." for balance '$group'\n";
		}
	    }
	}
	waitpid($ledger_pid, 0);
	return $balances;
    }
    # am the child; use STDIN/STDOUT normally

    run($config, $compta, %options);
    exit(0);
}

1;




    
