package Copro::Compta::Exercices;
use Moose;
use namespace::sweep;

has '_exercices' => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef[Copro::Compta::Exercice]',
    default   => sub { {} },
    handles   => {
	register       => 'set',
	exercice       => 'get',
	all            => 'values',
	count          => 'count',
	option_pairs   => 'kv',
	_all_exercices => 'values',
    },
    );

sub load {
    my $self = shift;
    
    map { $_->load ; } $self->exercices;
}

sub exercices {
    my $self = shift;

    return (sort { $a->id cmp $b->id } $self->_all_exercices);
}

sub getByDate {
    my $self = shift;
    my $date = shift;

    for my $exercice ($self->_all_exercices) {
	if ($date ge $exercice->date_start
	    && $date le $exercice->date_end) {
	    return $exercice;
	}
    }
    die "Cannot find exercice at $date (is the exercice loaded?)";
}
    

#around 'get' => sub {
#    my $orig = shift;
#    my $self = shift;
#    my $name = shift;
#
#    if (!defined($name) || $name eq '') {
#	$name = '2015'; # TODO
#    }
#    
#    if (! exercices->exists($name)) {
#	my $e = Copro::Compta::Exercice->new($name);
#	$self->exercices->set($name => $e);
#    }
#
#    return $self->$orig($name, @_);
#};

__PACKAGE__->meta->make_immutable;

1;
