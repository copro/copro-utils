package Copro::Compta::Config;
use strict;
use warnings;

our (@ISA, @EXPORT_OK);
BEGIN {
    require Exporter;
    @ISA = qw(Exporter);
    @EXPORT_OK = qw(load setconf getconf);
}

use Log::Log4perl qw(:easy);
use Config::Simple;
use File::BaseDir qw/:lookup/;
use File::Spec::Functions;
use Cwd qw/realpath/;

sub load($) {
    my $conffile=shift;
    
    my @configfiles=config_files(catfile('copro-compta', $conffile));

    if (-f $conffile) {
	unshift(@configfiles, $conffile);
    }

    my %config;
    for my $file (reverse @configfiles) {
	my $lcfg;
	my $cfg;
	$cfg = new Config::Simple(syntax=>"ini", filename=>$file)
	    or die $cfg->error();
	#$cfg->read($file) or die $cfg->error();
	my %lconfig=$cfg->vars();
	@config{ keys %lconfig } = values %lconfig;
    }
    return \%config;
}			 

my $config;
my $configdefault = {
    "default.financial-year"=>sub {
	use Time::Piece;

	my $t = Time::Piece->new();
	return $t->year;
    },
};

sub setconf($$$) {
    my $section=shift;
    my $ovar=shift;
    my $value=shift;
    my $var=$section.".".$ovar;
    $configdefault->{$var}=$value;
}

sub getconf($$) {
    my $section=shift;
    my $ovar=shift;
    my $var=$section.".".$ovar;
    if (defined($config->{$var})) {
	return $config->{$var};
    } elsif (defined($configdefault->{$var})) {
	my $type=ref($configdefault->{$var});
	if ($type eq "") {
	    return $configdefault->{$var};
	} elsif ($type eq "CODE") {
	    return $configdefault->{$var}->($section, $ovar);
	}
	die "internal error, invalid type $type for $section/$var";
    } else {
	return undef;
    }
}

1;
