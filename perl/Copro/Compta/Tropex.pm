package Copro::Compta::Tropex;
use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;

enum 'TropexType', [qw(CHTRAVAUX CHEXCEPT)];

with qw(Copro::Compta::Roles::ComptaAttr);

has 'tag' => (
    is  => 'ro',
    isa => 'Str',
    required => 1,
    );

has 'label' => (
    is  => 'ro',
    isa => 'Str',
    required => 1,
    );

has 'start' => (
    is => 'ro',
    isa => 'Copro::Compta::Exercice',
    required => 1,
    );

has 'end' => (
    is => 'rw',
    isa => 'Copro::Compta::Exercice',
    required => 0,
    predicate => 'done',
    );

has 'type' => (
    is  => 'ro',
    isa => 'TropexType',
    required => 1,
    );

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;
    my %args = (@_);

    my $compta = $args{'compta'};
    if (!defined($args{'end'}) || $args{'end'} eq '') {
	delete($args{'end'});
    } else {
	$args{'end'}=$compta->exercice($args{'end'});
    }
    $args{'start'}=$compta->exercice($args{'start'});
    
    return $class->$orig(%args);
};


1;
