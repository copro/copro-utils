package Copro::Compta::CMD::Ledger;
use Moose;
use namespace::sweep;

use Readonly;

with 'Copro::Compta::Roles::CMD';

sub synopsis {
    return "Appel de ledger lui-même";
}

sub options {
    return [];
}

Readonly my $REGISTER_FORMAT =>
    '%(ansify_if('.
    '  ansify_if(justify(format_date(date), int(date_width)),'.
    '            green if color and date > today),'.
    '            bold if should_bold))'.
    ' %(ansify_if('.
    '   ansify_if(justify(truncated(payee, int(payee_width)), int(payee_width)), '.
    '             bold if color and !cleared and actual),'.
    '             bold if should_bold))'.
    ' %(ansify_if('.
    '   ansify_if(justify(truncated(display_account+" "+account.note, int(account_width), '.
    '                               int(abbrev_len)), int(account_width)),'.
    '             blue if color),'.
    '             bold if should_bold))'.
    ' %(ansify_if('.
    '   justify(scrub(get_at(display_amount, 0)), int(amount_width), '.
    '           3 + int(meta_width) + int(date_width) + int(payee_width)'.
    '             + int(account_width) + int(amount_width) + int(prepend_width),'.
    '           true, color),'.
    '           bold if should_bold))'.
    ' %(ansify_if('.
    '   justify(scrub(get_at(display_total, 0)), int(total_width), '.
    '           5 + int(meta_width) + int(date_width) + int(payee_width)'.
    '             + int(account_width) + int(amount_width) + int(amount_width) + int(total_width)'.
    '             + int(prepend_width), true, color),'.
    '           bold if should_bold))\n%/'.
    '%(justify(" ", int(date_width)))'.
    ' %(ansify_if('.
    '   justify(truncated(has_tag("Payee") ? payee : " ", '.
    '                     int(payee_width)), int(payee_width)),'.
    '             bold if should_bold))'.
    ' %$3 %$4 %$5\n';
    
Readonly my $BALANCE_FORMAT =>
    '%(ansify_if('.
    '  justify(scrub(display_total), 20,'.
    '          20 + int(prepend_width), true, color),'.
    '            bold if should_bold))'.
    '  %(!options.flat ? depth_spacer : "")'.
    '%-(ansify_if('.
    '   ansify_if(partial_account(options.flat)+"  "+account.note, blue if color),'.
    '             bold if should_bold))\n%/'.
    '%$1\n%/'.
    '%(prepend_width ? " " * int(prepend_width) : "")'.
    '--------------------\n';

sub start_args {
    return ();
}

sub mid_args {
    return ();
}

sub end_args {
    return ();
}

sub run($$) {
    my $self=shift;
    my $config=shift;
    my $args=$config->internal->get('args');

    my @ledgerargs=
	('ledger',
	 $self->start_args(),
	 '-f', 'structure.ledger',
	 (map { ( '-f', $_->path ) } $self->compta->select_ledgers(
	      $config,
	      'kind' => ['pc'],
	  )),
	 '--pedantic',
	 '--sort', 'date',
	 '--account-width', '30',
	 '--meta-width', '20',
	 '--payee-width', '60',
	 '--date-format', '%Y-%m-%d',
	 '--balance-format', $BALANCE_FORMAT,
	 '--register-format', $REGISTER_FORMAT,
	 $self->mid_args(),
	 (map { ( '-f', $_->path ) } $self->compta->select_ledgers(
	      $config,
	      'kind' => ['journal'],
	  )),
	 $self->end_args(),
	 @{$args} );

    $self->execLedger(@ledgerargs);
    #TRACE @ledgerargs;
    #print STDERR "RUN: '", join("' '", @ledgerargs), "'\n";
    #exec @ledgerargs or die "Cannot execute ledger\n";
}

sub execLedger {
    my $self=shift;
    my @ledgerargs=@_;
    #TRACE @ledgerargs;
    print STDERR "RUN: '", join("' '", @ledgerargs), "'\n";
    exec @ledgerargs or die "Cannot execute ledger\n";
}

1;
