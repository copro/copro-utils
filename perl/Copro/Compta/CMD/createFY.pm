package Copro::Compta::CMD::createFY;
use strict;
use warnings;
use Log::Log4perl qw(:easy);
use Copro::Compta::Config;
use Copro::Compta::Ledger;
sub run($$) {
    my $cmd=shift;
    my $global_options=shift;
    my $p = new Getopt::Long::Parser
	config => ['require_order'];
    my $newname;
    
    $p->getoptionsfromarray(\@ARGV, @{$global_options});

    my $exercice=getconf("default", "financial-year");
    
    if (scalar(@ARGV) > 1) {
	die "Too many arguments for $cmd\n";
    }
    if (scalar(@ARGV) == 1) {
	$newname=$ARGV[1];
    } else {
	$newname=$exercice+1;
    }
    TRACE "creation de l'exercice comptable $newname";
    
    my $newdir=Exercice::dir($newname);
    if (not -d $newdir) {
	TRACE "creation du répertoire pour $newname";
	mkdir($newdir) or die "Cannot create directory $newdir: $!\n";
    }
    my $PCname=Journal::filename("PC", $newname);
    if (! -f $PCname) {
	TRACE "creation du plan comptable de $newname";
	my $curPCfile=Journal::filename("PC");
	if (! -f $curPCfile) {
	    die "Cannot create $PCname as current financial year directory does not have $curPCfile\n";
	}
	open(FH, '>', $PCname) or die "Cannot write into $PCname: $!\n";
	my $rel_path = File::Spec->abs2rel(realpath($curPCfile),$newdir);
	TRACE "dest=$rel_path";
	print FH "include $rel_path\n";
	close FH or die "Cannot write into $PCname: $!\n";
    }
}
    
1;
