package Copro::Compta::CMD::LaTeXAppel;
use Moose;
use namespace::sweep;
use Copro::Types;
use Math::BigRat;
use Path::Class::File;
use Copro::Utils::Amount qw(val_str amount_round);
use Copro::Utils::LaTeX qw(latexize);
use Copro::Utils::Ledger qw(set_amount set_rcdate set_knowndate);
use List::Util qw(sum uniqstr);

=pod

=encoding UTF-8

=cut
extends 'Copro::Compta::SubCMD';

with (
    'Copro::Compta::Roles::CMD',
    'Copro::Utils::Roles::Date',
    'Copro::Compta::Roles::Appel',
    'Copro::Compta::Roles::Account',
    'Copro::Compta::Roles::ReadJournals',
    'Copro::Compta::Roles::ReadCopro',
    'Copro::Compta::Roles::ExtraitCompte',
    );

sub synopsis {
    return "génère les données LaTeX pour un appel de fond";
}

use utf8;

has 'name' => (
    is      => 'rw',
    isa     => 'Str',
    documentation => qq{nom de l'appel à générer},
    predicate => 'has_name',
    );

has 'extload' => (
    is      => 'rw',
    isa     => 'Bool',
    documentation => qq{nom de l'appel à générer},
    default => 0,
    );

sub setfromvalue {
    my $refvar = shift;
    my $value = shift;
    
    if (defined($$refvar)) {
	if ($$refvar ne $value) {
	    print STDERR "W: $$refvar instead of $value!";
	}
    } else {
	$$refvar=$value;
    }
}
    
sub setfromtag {
    my $refvar = shift;
    my $tag = shift;
    my $obj = shift;

    return if not $obj->has_tag($tag);

    my $value=$obj->tag($tag);
    if (defined($$refvar)) {
	if ($$refvar ne $value) {
	    die "E: tag $tag is $value (not ".$$refvar.") in\n$obj\n";
	}
    } else {
	$$refvar=$value;
    }
}

sub max_str {
    my $max;
    for my $s (@_) {
	next if not defined($s);
	if (!defined($max) || $s gt $max) {
	    $max = $s;
	}
    }
    return $max;
}

sub LaTeXDumpAppel {
    my $self = shift;
    my $name = shift;
    my $options = { @_ };
    my $config = $options->{'config'} // die "E: config missing\n";
    my $transactions = $options->{'transactions'} // die "E: transactions missing\n";
    my $journal = $options->{'journal'} // die "E: journal missing\n";
    my $accounts_cache = $options->{'accounts-cache'} // {};

    my $accounts = $options->{'accounts'} 
    // $self->loadAccountsFromJournal($journal);

    my $str="%\n% Appel $name\n%\n";

    #####################################################################
    # Récupération des infos des transactions
    my $appelrcstartdate;
    my $appeldate;
    my $appeltitle;
    my $appeldateexigible;
    my $appelrcyear;
    my $data;
    my $appelconcern;
    foreach my $t (@$transactions) {
	my $transactionconcern={};
	setfromtag(\$appelrcstartdate, 'APPEL-RC-START-DATE', $t);
	setfromtag(\$appeldate, 'APPEL-DATE', $t);
	setfromtag(\$appeltitle, 'APPEL-TITLE', $t);
	set_rcdate($t);
	set_knowndate($t);
	my $rcdate=$t->var("rcdate_str");
	$appeldateexigible=max_str($appeldateexigible, $rcdate);
	my $tdata = {
	    #'transaction' => $t,
	    'label' => $t->tag('APPEL-LABEL') // latexize($t->description),
	    'datedesc' => $t->date_str,
	    'concern' => $transactionconcern,
	};
	my $iter_p = $t->iterator(
	    'select-element' => sub {
            return shift->isa("Ledger::Posting");
	    });
	while (defined(my $p=$iter_p->next)) {
	    my $account=$accounts->get($p->account->name);
	    if (not defined($account)) {
		die "Invalid account name ".$p->account->name;
	    }
	    my $n=$account->name;
	    my $keytype=$p->tag('REPKSTYPE');
	    if (defined($keytype) && $keytype eq 'IGNORE') {
		next;
	    } elsif ($n =~ /^7:/) {
		push @{$tdata->{'repart'}}, {
		    'amount' => $p->amount->amount,
		    'key' => $p->tag('REPCLEF') // 'NONE',
		    'datekey' => $p->tag('REPCLEFDATE') // undef,
		};
		if ($p->has_tag('REPKSTYPE')) {
		    setfromtag(\$tdata->{'keytype'}, 'REPKSTYPE', $p);
		}
		if ($p->has_tag('REPKSTOTTANTIEMES')) {
		    setfromtag(\$tdata->{'tottantiemes'}, 'REPKSTOTTANTIEMES', $p);
		}
	    } elsif ($n =~ /^4:5:0:/) {
		$tdata->{'partcoprop'}->{$n} -= $p->amount->amount;
		if ($p->has_tag('REPKSATTRIB')) {
		    setfromtag(\$tdata->{'attribcoprop'}->{$n}, 'REPKSATTRIB', $p);
		}
		if ($p->has_tag('REPKSTANTIEMES')) {
		    setfromtag(\$tdata->{'tantiemescoprop'}->{$n}, 'REPKSTANTIEMES', $p);
		}
		$appelconcern->{$n}=1;
		$transactionconcern->{$n}=1;
	    } elsif ($n =~ /ROMPUS/) {
		# skipping
	    } else {
		warn "W: Strange account $n for $name\n";
	    }
	}
	push @{$data}, $tdata;
    }
    if (not defined($appeltitle)) {
	warn "W: No title for $name (missing APPEL-TITLE?)";
	$appeltitle=$data->[0]->{'label'};
    }
    if (not defined($appeldate)) {
	warn "W: No date for $name (missing APPEL-DATE?)";
	$appeltitle=$data->[0]->{'label'};
    }
    $str .= "\\coproAPFdefine{%\n".
	"  name={$name},%\n".
	"  concern={".join(',', keys %$appelconcern)."},%\n".
	"  title={$appeltitle},%\n".
	"  appeldate={$appeldate},%\n".
	"  coprodate={".$self->loadCoproDesc(
	    $self->descdir, {
		'default' => $appeldate,
	    })->date_coprop_real."},%\n".
	"  exigibledate={$appeldateexigible},%\n".
	"}{%\n";
    #####################################################################
    # Génération de chaque transaction
    foreach my $t (@$data) {
	my $byaccount;
	my $params;
	$str.="  \\coproAPFtransaction{%\n".
	    "    entryname={".$t->{'label'}."},%\n".
	    "    concern={".join(',', sort (keys %{$t->{'concern'}}))."},%\n";
	my $desc;
	my $dd=$t->{'datedesc'};
	my $dt;
	#####################################################################
	# Pour chaque clé
	my @reparts=map {
	    my $r=$_;
	    if (not defined($desc)) {
		$dt=$r->{'datekey'} // $appeldate;
		$desc = $self->loadCoproDesc(
		    $self->descdir, {
			'default' => $dd,
			#'keys' => $r->{'datekey'} // $dd,
			'tantiemes' => $dt,
		    });
		$str .= "    configdate={".$desc->subdir($dd)."},%\n";
		if ($dd ne $dt) {
		    $str .= "    keyconfigdate={".$desc->subdir($dt)."},%\n";
		}
		$str .= "  }{%\n";
	    } else {
		if ($desc->date_tantiemes ne ($dt)) {
		    die "Invalid datekey for $name";
		}
	    }
	    # Local $str
	    my $str='';
	    my $key = $r->{'key'};
	    my $keyinfo=$desc->info('keys', 'K.Id', $key);
	    my $keylabel=$keyinfo->elm(0, 'K.Label');
	    my $keynum=$keyinfo->elm(0, 'K.num');
	    my $table=$desc->info('lotstantiemescoprop', 'K.Id', $key);
	    my $keytype=$t->{'keytype'};
	    my $tantiemes;
	    my @accs;
	    if (defined($keytype)) {
		# Cas spéciaux
		if ($keytype eq 'LOTS') {
		    $key = 'LOTS';
		    $keylabel = "Répartition par lot (%\n";
		    foreach my $c (sort (keys %{$t->{'attribcoprop'}//[]})) {
			$_ = $t->{'attribcoprop'}->{$c};
			$keylabel .= "        \\coproAPFfor{$c}{".join(', ', split())."}%\n";
		    }
		    $keylabel .= "      )";
		    $tantiemes=$t->{'tottantiemes'} // die "Missing tag REPKSTOTTANTIEMES in $name";
		    @accs=keys %{$t->{'tantiemescoprop'}//[]};
		} elsif ($keytype eq 'COPROP') {
		    $key = 'COPROP';
		    $keylabel = "Charge individuelle%\n";
		    $tantiemes=1;
		    @accs=keys %{$t->{'partcoprop'}//[]};
		} else {
		    die "E: unsupported REPKSTYPE $keytype";
		}
	    } else {
		$params->{'amounts'}->{$key}+=$r->{'amount'};
		$tantiemes=sum($table->col('tantiemes'))//($r->{'amount'}-$r->{'amount'});
		@accs=uniqstr $table->col('Account');
	    }
		
	    $str.="    \\coproAPFrepart{%\n".
		"      amount={".val_str($r->{'amount'})."},%\n".
		"      tantiemes={".$tantiemes."},%\n".
		"      title={$keylabel},%\n".
		"      concern={".join(',', sort @accs)."},%\n".
		"    }{%\n";
	    if (defined($keytype)) {
		if ($keytype eq 'LOTS') {
		    my $sumtantiemes=0;
		    foreach my $c (sort (keys %{$t->{'tantiemescoprop'}//[]})) {
			$str .= "      \\coproAPFfor{$c}{".
			    $t->{'tantiemescoprop'}->{$c}."}%\n";
			$byaccount->{$c} += amount_round($r->{'amount'} *
							 $t->{'tantiemescoprop'}->{$c} /
							 $t->{'tottantiemes'});
			$sumtantiemes += $t->{'tantiemescoprop'}->{$c};
		    }
		    if ($sumtantiemes != $t->{'tottantiemes'}) {
			die "Invalid tantiemes in $name";
		    }
		} elsif ($keytype eq 'COPROP') {
		    foreach my $c (sort (keys %{$t->{'partcoprop'}//[]})) {
			$str .= "      \\coproAPFfor{$c}{1}%\n";
			$byaccount->{$c} += $t->{'partcoprop'}->{$c};
		    }
		} else {
		    die "E: unsupported REPKSTYPE $keytype";
		}
	    } else {
		foreach my $c (@accs) {
		    $str .= "      \\coproAPFfor{$c}{".
			sum($desc
			    ->infotable($table,'Account',$c)
			    ->col('tantiemes'))."}%\n";
		}
	    }
	    $str.="    }%\n";
	    { 'str' => $str,
	      'num' => $keynum,
	    }
	} @{$t->{'repart'}};
	$str.=join('', map { $_->{'str'} } (sort {$a->{'num'} <=> $b->{'num'} } @reparts));
	$str.="  }{%\n";
	$params->{'copro'} = $desc;
	#####################################################################
	# Dans chaque transaction, la part des copropriétaires
	my $results=$self->appelCompute(%$params);
	foreach my $co (@{$results->{'coprop'}}) {
	    $byaccount->{$co->{'copro'}->{'Account'}}+=$co->{'amount'};
	}
	foreach my $c (sort (keys %{$t->{'partcoprop'}//[]})) {	    
	    $str.="    \\coproAPFfor{$c}{".
		val_str($t->{'partcoprop'}->{$c}).
		"}%";
	    if (not defined($byaccount->{$c})) {
		$str.=" **** WARNING: absent by calcul";
		warn "W: missing account $c in $name";
	    } elsif ($t->{'partcoprop'}->{$c} != $byaccount->{$c}) {
		$str.=" **** WARNING: calcul: ".val_str($byaccount->{$c});
		warn "W: wrong sum for account $c in $name (".
		    val_str($t->{'partcoprop'}->{$c})." instead of ".
		    val_str($byaccount->{$c}).") -> delta=".
		    val_str($t->{'partcoprop'}->{$c}-$byaccount->{$c})."\n";
	    }
	    $str.="\n";
	}
	$str .= "  }%\n";
    }
    $str .= "}%\n";

    #####################################################################
    # Le relevé de compte associé à l'appel
    if (not defined($appelrcstartdate)) {
	$appelrcstartdate=$self->compta->exercices->getByDate($appeldate)->date_start;
    }
    if ($appelrcstartdate eq "-1") {
	$appelrcstartdate=$self->compta->exercices->getByDate($appeldate)->prev->date_start;
    }
    # checking avaibility of exercices
    my $ex_rcstart=$self->compta->exercices->getByDate($appelrcstartdate);
    my $ex_rcfirst;
    if (not defined($ex_rcstart->prev)) {
	warn "W: previous exercice is missing for $name\n";
	$ex_rcfirst=$ex_rcstart;
    } else {
	$ex_rcfirst=$ex_rcstart->prev;
    }
    if ($appeldateexigible lt $appeldate) {
	die "In APF $name, payment required before call ($appeldateexigible < $appeldate)\n";
    }
    my $ex_rclast_id=$self->compta->exercices->getByDate($appeldateexigible)->id;

    my @ex_rc = ($ex_rcfirst);
    while ($ex_rc[-1]->id ne $ex_rclast_id) {
	push @ex_rc, $ex_rc[-1]->next;
    }
    
    my $journal_rc=$self->load_journals($config, {
	'exercices' => \@ex_rc,
					});
    if (!exists($accounts_cache->{$ex_rclast_id})) {
	$accounts_cache->{$ex_rclast_id}=$self->loadAccountsFromJournal($journal_rc);
    }
    my $accounts_rc=$accounts_cache->{$ex_rclast_id};
    
    $str .= "% rc-start: $appelrcstartdate\n".
	"% rc-end: $appeldateexigible\n";

    $str .= $self->LaTeXExtraitCompte(
	'journal' => $journal_rc,
	'accounts' => $accounts_rc,
	'date-start' => $appelrcstartdate,
	'date-end' => $appeldateexigible,
	'date-limit' => $appeldate,
	'accounts-regexp' => '^4:5:0:',
	'ns' => 'copro@APF@RC@'.$name,
	);

    return {
	'appeldate' => $appeldate,
	'exigibledate' => $appeldateexigible,
	'contents' => $str,
    };
}


sub run($$) {
    my $self=shift;
    my $config=shift;
    
    my @exercices= $config->section("default")->exercices;
    @exercices=sort {
	$a->id cmp $b->id
    } @exercices;
    my $datemin=$exercices[0]->date_start;
    
    my $journal=$self->load_journals($config);

    my $accounts=$self->loadAccountsFromJournal($journal);
    my $appels=$self->find_appels($journal, $accounts);

    my %params = (
	'config' => $config,
	'date-min' => $datemin,
	'journal' => $journal,
	'accounts' => $accounts,
	);    
    binmode( STDOUT, ":utf8");
    if ($self->has_name) {
	if (not exists($appels->{$self->name})) {
	    print STDERR "L'appel '".$self->name."' n'a pas été trouvé.\n";
	    print STDERR "Appels disponibles:\n";
	    print STDERR "- ".join("\n- ", sort (keys %$appels))."\n";
	    exit(1);
	}
	print $self->LaTeXDumpAppel(
	    $self->name,
	    'transactions' => $appels->{$self->name},
	    %params)->{'contents'};
    } else {
	my $accounts_cache={};
	my @APF=map {
	    $self->LaTeXDumpAppel(
		$_,
		'transactions' => $appels->{$_},
		'accounts-cache' => $accounts_cache,
		%params)
	} (keys %$appels);
	print join('', 
		   map { $_->{'contents'} }
		   (sort { $a->{'appeldate'} cmp $b->{'appeldate'}
		    || $a->{'exigibledate'} cmp $b->{'exigibledate'} } @APF))
    }
}

1;
