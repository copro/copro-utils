package Copro::Compta::CMD::Register;
use Moose;
use namespace::sweep;
use Copro::Compta::Register;

with 'Copro::Compta::Roles::CMD';

sub synopsis {
    return "liste des transactions pour les annexes"
}

sub options {
    return [];
}

my %local_register = (
);

sub start_args {
    return ();
}

sub mid_args {
    return ();
}

sub end_args {
    return ();
}

sub run($$) {
    my $self=shift;
    my $config=shift;
    my $args=$config->internal->get('args');
    
    foreach my $exercice ($config->default->exercices) {
	$exercice->load;
    }
    
    my $annexe=$args->[0];
    my @annexes = ($annexe);
    
    if (! defined($annexe) || $annexe eq "all") {
	@annexes = Copro::Compta::Register::getSupportedAnnexes();
    }

    foreach $annexe (@annexes) {
	my $params=Copro::Compta::Register::getParamsForAnnexe(
	    $annexe, %local_register);
	if (! defined($params)) {
	    print STDERR "Unknown annexe '$annexe'\n";
	    print STDERR "Choose between '"
		.join("', '", 
		      Copro::Compta::Register::getSupportedAnnexes(%local_register)
		)."'\n";
	    exit(1);
	}
	#print "\n\nComptes pour l'annexe $annexe\n\n";
	Copro::Compta::Register::run(
	    $config,
	    $self->compta,
	    'mid-args' => $params,
	    'end-args' => [ '--flat' ],
	    );
    }

    return;
}

1;
