package Copro::Compta::CMD::ExtraitCompte;
use Moose;
use namespace::sweep;
use Copro::Types;
use Math::BigRat;
use Path::Class::File;

=pod

=encoding UTF-8

=cut
extends 'Copro::Compta::SubCMD';

with (
    'Copro::Compta::Roles::CMD',
    'Copro::Utils::Roles::Date',
    'Copro::Compta::Roles::Account',
    'Copro::Compta::Roles::ReadJournals',
    'Copro::Compta::Roles::ExtraitCompte',
    );

sub synopsis {
    return "génère des extraits de comptes";
}

has 'date-start' => (
    is      => 'rw',
    isa     => 'Str',
    #builder => 'today',
    #lazy    => 1,
    accessor => 'dstart',
    documentation => qq{date de début des extraits de compte},
    predicate => 'has_dstart',
    );

has 'date-end' => (
    is      => 'rw',
    isa     => 'Str',
    #builder => 'today',
    #lazy    => 1,
    accessor => 'dend',
    documentation => qq{date de fin des extraits de compte},
    predicate => 'has_dend',
    );

has 'date-limit' => (
    is      => 'rw',
    isa     => 'Str',
    #builder => 'today',
    #lazy    => 1,
    accessor => 'dlimit',
    documentation => qq{date limite de l'extrait définitif},
    predicate => 'has_dlimit',
    );

has 'accounts' => (
    is      => 'rw',
    isa     => 'Str',
    accessor => 'accounts_regexp',
    documentation => qq{expression régulière des comptes à extraire},
    default => '^4:5:0'
);

has '_ns' => (
    is      => 'rw',
    isa     => 'Str',
    accessor => 'ns',
    predicate => 'has_ns',
    clearer => 'unset_ns',
);

#use Data::Dumper;
#$Data::Dumper::Useperl = 1;
#$Data::Dumper::Sortkeys = \&my_filter;
#sub my_filter {
#    my ($hash) = @_;
#    return [ (sort grep { ! /parent/ } keys %$hash) ];
#}
use Ledger::Util::Filter ':constants';
use sort 'stable';

sub run($$) {
    my $self=shift;
    my $config=shift;
    
    my $journal=$self->load_journals($config, {
	'set-dstart' => 1,
	'set-dend' => 1,
	'load-prev-exercice' => 1,
				     });

    my $accounts=$self->loadAccountsFromJournal($journal);

    my @options;
    if ($self->has_ns) {
	push @options, 'ns', $self->ns;
    }
    if ($self->has_dlimit) {
	push @options, 'date-limit', $self->dlimit;
    }
    my $str=$self->LaTeXExtraitCompte(
	'journal' => $journal,
	'accounts' => $accounts,
	'date-start' => $self->dstart,
	'date-end' => $self->dend,
	'accounts-regexp' => $self->accounts_regexp,
	@options,
	);

    binmode( STDOUT, ":utf8");
    print $str;
}

1;
