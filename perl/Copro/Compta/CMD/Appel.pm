package Copro::Compta::CMD::Appel;
use Moose;
use namespace::sweep;
use Copro::Types;
use Math::BigRat;
use Path::Class::File;
use Copro::Utils::Amount qw(val_str amount_round);

=pod

=encoding UTF-8

=cut
extends 'Copro::Compta::SubCMD';

with (
    'Copro::Compta::Roles::CMD',
    'Copro::Utils::Roles::Date',
    'Copro::Compta::Roles::Appel',
    'Copro::Compta::Roles::ReadCopro',
    );

use Data::Dumper;
$Data::Dumper::Useperl = 1;

sub synopsis {
    return "génère un appel de fonds depuis un budget";
}

has 'date' => (
    is      => 'rw',
    isa     => 'Str',
    #builder => 'today',
    #lazy    => 1,
    documentation => qq{Remplace la date de la transaction},
    predicate => 'date_forced',
    );

has 'budget-file' => (
    is      => 'rw',
    isa     => 'Path::Class::File',
    accessor => 'budgetfile',
    documentation => qq{Fichier budget à prendre en compte},
    coerce  => 1,
    predicate => 'has_budgetfile',
    );

has 'factor' => (
    is      => 'rw',
    isa     => 'Math::BigRat',
    coerce => 1,
    default => sub { return Math::BigRat->new(1); },
    documentation => qq{Proportion du budget à appeler},
    );

has 'label' => (
    is      => 'rw',
    isa     => 'Str',
    documentation => qq{Remplace le label de la transation},
    predicate => 'label_forced',
    );

has 'amount' => (
    is      => 'rw',
    isa     => 'Math::BigRat',
    coerce => 1,
    documentation => qq{Remplace la somme à répartir dans la transaction},
    predicate => 'amount_forced',
    );

use Data::Dumper;
$Data::Dumper::Sortkeys = \&my_filter;
sub my_filter {
    my ($hash) = @_;
    return [ (sort grep { ! /parent/ } keys %$hash) ];
}
use Ledger::Util::Filter ':constants';
sub run($$) {
    my $self=shift;

    my $config=shift;
    my $args=$config->internal->get('args');
    my @exercices=$config->section("default")->exercices;

    #map { $_->load; } @exercices;

    if (!$self->has_budgetfile) {
	print STDERR "Budget file required\n";
	exit 1;
    }
    
    Copro::Compta::Ledger->_parser->read_file($self->budgetfile);
    
    my $journal_exercice="";
    foreach my $ledger (
	$self->compta->select_ledgers(
	    $config,
	    'exercices' => undef, #$options{'exercices'},
	    'kind' => ['pc'],
	    #'load' => 1,
	),
	) {
	$journal_exercice .= "include ".$ledger->path."\n";
    }
    $journal_exercice .= "include ".$self->budgetfile."\n";
    my $journal=Copro::Compta::Ledger->_parser->read_string($journal_exercice);

    my $iter_t=$journal->iterator(
	'follow-includes'=>1,
	'select-element' => sub {
            return shift->isa("Ledger::Transaction");
        });

    binmode( STDOUT, ":utf8");

    my $jres=Ledger::Parser->new()->read_string("");
    
    while (defined(my $t=$iter_t->next)) {
        #print $t;
	my $ht={ $t->toHash };
	if ($self->label_forced) {
	    $ht->{'description'} = $self->label;
	} else {
	    my $prefix;
	    if ($self->factor > 0) {
		$prefix="Appel";
	    } else {
		$prefix="Annulation";
	    }
	    my $f=abs($self->factor)*100;
	    if ($f != 100) {
		$prefix .= " (".val_str($f)."%)";
	    }
	    $ht->{'description'} = $prefix." : ".$ht->{'description'};
	}
	delete($ht->{'elements'});
	my $nt=$jres->add("Transaction", $ht);
	if ($self->date_forced) {
	    $nt->date($self->date);
	}
	my $date_copro=$nt->date_str();
	
	my $iter_e=$t->iterator(
	    'filter-out-element' => sub {
		return FILTERSUB;
	    });
	my $amounts={};
	while (defined(my $e=$iter_e->next)) {
	    if ($e->isa("Ledger::Posting")) {
		my $p=$e;
		my $account=$p->account->name;
		next if ($account !~ /^7/);
		#print "P: ", $p->account->name, " ", $p->amount->amount, "\n";
		my $curkey=$p->tag('REPCLEF');
		if (not defined($curkey)) {
		    print STDERR "Warning: skipping the following posting with no repartition key\n";
		    print STDERR $p;
		}
		my $amount=amount_round($p->amount->amount,$self->factor);
		if ($self->amount_forced) {
		    # TODO: FIXME: allow to introduce several amount...
		    $amount=$self->amount;
		}
		my $np=$nt->copy($p);
		$np->amount->amount($amount);
		$np->account->kind(Ledger::Posting::Kind::REAL);
		$amounts->{$curkey} += $amount;
	    } elsif ($e->isa("Ledger::Transaction::Note")) {
		if ($e->note->isa("Ledger::Value::SubType::TaggedValue")) {
		    my $t=$e->note;
		    if ($t->name eq 'PHASE') {
			next;
		    }
		}
		$nt->copy($e);
	    } else {
		print STDERR "Strange element in transaction: ", $e;
	    }
	}

	my $appel=$self->appelCompute(
	    'copro' => $self->loadCoproDesc($self->descdir, $date_copro),
	    'amounts' => $amounts,
	    'factor' => $self->factor,
	    );

	#$ht={ $t->toHash };
	#print "t hashed\n", Dumper($ht);
	
	$self->appel2postings($nt, $appel);

	print $nt,"\n";

    }
}

1;
