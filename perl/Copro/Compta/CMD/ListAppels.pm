package Copro::Compta::CMD::ListAppels;
use Moose;
use namespace::sweep;

=pod

=encoding UTF-8

=cut
extends 'Copro::Compta::SubCMD';

with (
    'Copro::Compta::Roles::CMD',
    'Copro::Compta::Roles::Appel',
    'Copro::Compta::Roles::ReadJournals',
    'Copro::Compta::Roles::Account',
    );

use Data::Dumper;
$Data::Dumper::Useperl = 1;
$Data::Dumper::Sortkeys = \&my_filter;
sub my_filter {
    my ($hash) = @_;
    return [ (sort grep { ! /parent/ } keys %$hash) ];
}

sub synopsis {
    return "liste les appels de l'exercice";
}

use Ledger::Util::Filter ':constants';
sub run($$) {
    my $self=shift;

    my $config=shift;

    my $journal=$self->load_journals($config);
    my $accounts=$self->loadAccountsFromJournal($journal);

    my $appels=$self->find_appels($journal, $accounts);
    
    print join("\n", sort (keys %$appels)), "\n";
}

1;
