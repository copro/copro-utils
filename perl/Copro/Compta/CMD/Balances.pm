package Copro::Compta::CMD::Balances;
use Moose;
use namespace::sweep;
use Copro::Compta::Balances;

with 'Copro::Compta::Roles::CMD';

sub synopsis {
    return "Balances pour les annexes comptables";
}

sub options {
    return [];
}

my %local_balances = (
    'bilan' => [
	'--group-by',
	'"GROUP: " + ((tag("PHASE") == "CLOTURE")? "CLOTURE" : '
	.'(account=~/^[145]:/ ? "BILAN" : ("GESTION " + tag("REPTYPECH")))))',
	],
);

sub start_args {
    return ();
}

sub mid_args {
    return ();
}

sub end_args {
    return ();
}

sub run($$) {
    my $self=shift;
    my $config=shift;
    my $args=$config->internal->get('args');
    
    foreach my $exercice ($config->default->exercices) {
	$exercice->load;
    }
    
    my $annexe=$args->[0];
    my @annexes = ($annexe);
    
    if (! defined($annexe) || $annexe eq "all") {
	@annexes = Copro::Compta::Balances::getSupportedAnnexes();
    }

    foreach $annexe (@annexes) {
	my $params=Copro::Compta::Balances::getParamsForAnnexe(
	    $annexe, %local_balances);
	if (! defined($params)) {
	    print STDERR "Unknown annexe '$annexe'\n";
	    print STDERR "Choose between '"
		.join("', '", 
		      Copro::Compta::Balances::getSupportedAnnexes(%local_balances)
		)."'\n";
	    exit(1);
	}
	print "\n\nBalance pour l'annexe $annexe\n\n";
	Copro::Compta::Balances::run(
	    $config,
	    $self->compta,
	    'mid-args' => $params,
	    'end-args' => [ '--flat' ],
	    );
    }

    return;
    Copro::Compta::Balances::run(
	$config,
	$self->compta,
	'start-args' => [ $self->start_args() ],
	'mid-args' => [ $self->mid_args() ],
	'end-args' => [ $self->end_args() ],
	'args' => $args,
	);
}

1;
