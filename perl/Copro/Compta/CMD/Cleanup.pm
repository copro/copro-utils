package Copro::Compta::CMD::Cleanup;
use Moose;
use namespace::sweep;
use Ledger::Parser;
use Copro::Compta::Ledger;

extends 'Copro::Compta::SubCMD';

with (
    'Copro::Compta::Roles::CMD',
    );

sub synopsis {
    return "Netoyage des fichiers comptables";
}

sub options {
    return [];
}

sub run($$) {
    my $self=shift;
    my $config=shift;
    my $args=$config->internal->get('args');
    my @exercices=$config->section("default")->exercices;

    map { $_->load; } @exercices;

    foreach my $journal (Copro::Compta::Ledger->_parser->journals->all_journals) {
	if ($journal->is_file) {
	    print "Writing ".$journal->file."\n";
	    $journal->validate;
	    $journal->cleanup;
	    open JOURNAL, ">utf8", $journal->file or die "Cannot write ".$journal->file.": $!\n";
	    print JOURNAL $journal;
	    close JOURNAL;
	}
    }
}

1;
