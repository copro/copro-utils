package Copro::Compta::CMD::Annexes;
use Moose;
use namespace::sweep;
use Copro::Compta::Balances;
use List::Util qw(max);
use Carp;

with (
    'Copro::Compta::Roles::CMD',
    'Copro::Compta::Roles::Account',
    'Copro::Compta::Roles::Appel',
    );

sub synopsis {
    return "Données pour les annexes comptables";
}

sub options {
    return [];
}

use Data::Dumper;
use Math::BigRat;

sub latexAnnexeTotal {
    my $self = shift;
    my %options = (
	'part-offset' => 0,
	@_
	);
    
    my @cols = @{$options{'columns'}};
    my $nb_cols = scalar(@cols);
    my @parts = @{$options{'parts'}};
    my $nb_parts = scalar(@parts);
    my $prefix=$options{'prefix'};
    my $secname=$options{'secname'};

    my $res;

    $res .= "\n\\newcommand{\\${prefix}${secname}soustotal}{%\n";
    $res .= "  %linetype,partnum,linenum,[libelle,poste,sort,{val*$nb_cols}]\n";

    my $lg=1;
    for(my $npart=0; $npart < $nb_parts; ++$npart) {
	my $part=$parts[$npart];
	my $numpart=$npart+1+$options{'part-offset'};
	my $factor=$options{'factors'}->{$part} // 1;
	my $type = "total";

	$res .= "  \\${prefix}${type}{$type}{$numpart}{$lg}";
	$res .= "{}{}{}{";
	$res .= join(
	    '}{',
	    map {
		my $val=$options{'total'}->{$_}->{$part};
		if (not defined($val)) {
		    "\\tnoval";
		} else {
		    $val *= ($options{'factors'}->{$_} // 1);
		    "\\tval{".($val * $factor)->as_float."}";
		}
	    } @cols);
	$res .= "}\n";
	
	
	my $kindsep='Sep';
	if ($npart + 1 == $nb_parts) {
	    $kindsep='End';
	    $res .= "  ";
	}
	$res .= "  \\${prefix}${kindsep}Line{$type}{$numpart}{$lg}\n";
    }
    $res .= "}\n";
    return $res;
}

sub latexAnnexeBody {
    my $self = shift;
    my %options = (
	'print-total' => 1,
	'part-offset' => 0,
	@_
	);

    my $bals = $options{'values'};
    my @cols = @{$options{'columns'}};
    my $nb_cols = scalar(@cols);
    my @parts = @{$options{'parts'}};
    my $nb_parts = scalar(@parts);
    my $partlines = $options{'partlines'};
    my $accounts = $options{'accounts'};
    #print Dumper($partlines);
    #print Dumper(\@parts);
    my %nb_lines = map { $_ => scalar(keys %{$partlines->{$_}}) } @parts;
    my $nb_lines = max (values %nb_lines);
    my $prefix=$options{'prefix'};
    my $secname=$options{'secname'};
    
    my $res;

    $res .= "\n\\newcommand{\\${prefix}${secname}body}{%\n";
    $res .= "  %linetype,partnum,linenum,[libelle,poste,sort,{val*$nb_cols}]\n";
    my $sortedLines;
    my $total;
    foreach my $part (@parts) {
	my @lines = sort
	{ ($partlines->{$part}->{$a}->{'sortkey'} // $a)
	  cmp ($partlines->{$part}->{$b}->{'sortkey'} // $b) } 
	(keys %{$partlines->{$part}});
	$sortedLines->{$part}=\@lines;
    }
    for(my $lg=1; $lg <= $nb_lines; ++$lg) {
	for(my $npart=0; $npart < $nb_parts; ++$npart) {
	    my $part=$parts[$npart];
	    my $numpart=$npart+1+$options{'part-offset'};
	    my $factor=$options{'factors'}->{$part} // 1;
	    
	    my $type = "NA";
	    if ($lg > $nb_lines{$part}) {
		$res .= "  \\${prefix}EmptyEntry{$type}{$numpart}{$lg}\n";
	    } else {
		my $kline=$sortedLines->{$part}->[$lg - 1];
		my $line=$partlines->{$part}->{$kline};
		$type = $line->{'type'} // "notypefor $kline";
		$res .= "  \\${prefix}${type}{$type}{$numpart}{$lg}";
		$res .= "{".($line->{'libelle'} // "no label for $kline")."}{".($line->{'poste'}//'')
		    ."}{".($line->{'sortkey'} // $kline)."}{";
		$res .= join(
		    '}{',
		    map {
			my $val=$bals->{$_}->{$part}->{'accounts'}->{$kline};
			if (not defined($val)) {
			    "\\tnoval";
			} else {
			    $total->{$_}->{$part} += $val;
			    $val *= ($options{'factors'}->{$_} // 1);
			    "\\tval{".($val * $factor)->as_float."}";
			}
		    } @cols);
		$res .= "}\n";
	    }

	    my $kindsep='Sep';
	    if ($npart + 1 == $nb_parts) {
		$kindsep='End';
		$res .= "  ";
	    }
	    $res .= "  \\${prefix}${kindsep}Line{$type}{$numpart}{$lg}\n";
	}
    }
    $res .= "}\n";

    if ($options{'print-total'}) {
	$res .= $self->latexAnnexeTotal(
	    %options,
	    'total' => $total,
	    );
    }
    return $res;
}

sub normalizeBalances {
    my $self = shift;
    my $ibal = shift;
    my $obal;

    foreach my $bal (keys %{$ibal}) {
	foreach my $iacc (keys %{$ibal->{$bal}->{'accounts'}}) {
	    my $oacc = $iacc;
	    $oacc =~ s/:(.[^:]|[^0-9]).*//;
	    my $val = $ibal->{$bal}->{'accounts'}->{$iacc};
	    $obal->{$bal}->{'accounts'}->{$oacc} += $val;
	    $obal->{$bal}->{'total'} += $val;
	}
	foreach my $oacc (keys %{$obal->{$bal}->{'accounts'}}) {
	    my $val = $obal->{$bal}->{'accounts'}->{$oacc};
	    if ($val->is_zero) {
		delete($obal->{$bal}->{'accounts'}->{$oacc});
	    }
	}
    }
    return $obal;
}

sub listaccounts {
    my $self = shift;
    my %options = (
	'add-parents' => 1,
	@_,
	);
    my $res;
    my $accounts=$options{'accounts'};
    my @add = ();
    my @addentry = ();
    if (defined($options{'add'})) {
	foreach my $acc ($accounts->accounts) {
	    my $name=$acc->name;
	    if ($name =~ $options{'add'}) {
		push @add, $acc->name;
	    }
	}
    }
    if (defined($options{'add-entry'})) {
	foreach my $acc ($accounts->accounts) {
	    my $name=$acc->name;
	    if ($name =~ $options{'add-entry'}) {
		push @addentry, $acc->name;
	    }
	}
    }
    foreach my $account (@{$options{'base'}}, @add, @addentry) {
	my $acc=$accounts->byName($account);
	die "problem" if not defined($acc);
	while (defined($acc)) {
	    $res->{$acc->name}->{'type'} = 'label';
	    $res->{$acc->name}->{'libelle'} = $acc->note;
	    $res->{$acc->name}->{'poste'} = $self->accountname($acc->name);
	    $res->{$acc->name}->{'sortkey'} = $acc->name;
	    last if not $options{'add-parents'};
	    $acc=$acc->parent;
	}
    }
    foreach my $account (@{$options{'base'}}, @addentry) {
	my $acc=$accounts->byName($account);	
	$res->{$acc->name}->{'type'} = 'entry';
    }
    if (defined($options{'remove'})) {
	foreach my $account (keys %{$res}) {
	    if ($res->{$account}->{'type'} eq 'label'
		&& $account =~ $options{'remove'}) {
		delete($res->{$account});
	    }
	}
    }
    return $res;
}

sub annexe1 {
    my $self=shift;
    my $config=shift;
    my $exercice=shift;

    my $res;
    
    my $balances;
    my @a1parts=('TRESORERIE', 'AVANCES', 'CREANCES+DETTES');

    my %years = (
	'prev' => $exercice->prev,
	'cur' => $exercice,
	);
    for my $year ('prev', 'cur') {
	my $bals=Copro::Compta::Balances::get(
	    $config,
	    $self->compta,
	    'mid-args' => Copro::Compta::Balances::getParamsForAnnexe('annexe1'),
	    'end-args' => [ '--flat' ],
	    'ensure-balances' => \@a1parts,
	    'ledger-kind' => [ 'journal' ],
	    'exercices' => [ $years{$year} ],
	    );
	{
	    my $total;
	    foreach my $part (@a1parts) {
		$total += $bals->{$part}->{'total'};
	    }
	    if (!$total->is_zero) {
		print STDERR "Unbalanced exercice!\n";
	    }
	}
	
	my $cd=$a1parts[2];
	foreach my $account (keys %{$bals->{$cd}->{'accounts'}}) {
	    my $part = 'DETTES';
	    my $val = $bals->{$cd}->{'accounts'}->{$account};
	    if ($val->is_neg) {
		$part= 'CREANCES';
	    }
	    $bals->{$part}->{'accounts'}->{$account}=$val;
	    $bals->{$part}->{'total'} += $val;
	}
	delete($bals->{$cd});
	$balances->{$year}=$self->normalizeBalances($bals);
    }
    my $accounts=$self->loadAccountsFromConfig($config);
    my $partlines;
    @a1parts=('TRESORERIE', 'AVANCES', 'CREANCES', 'DETTES');
    my $zero = Math::BigRat->new(0);
    my $total;
    {
	my $col = {
	    'TRESORERIE' => 'COL1',
	    'AVANCES' => 'COL2',
	    'CREANCES' => 'COL1',
	    'DETTES' => 'COL2',
	};
	for my $year ('prev', 'cur') {
	    for my $part (@a1parts) {
		$total->{$year}->{$col->{$part}} +=
		    ( $balances->{$year}->{$part}->{'total'} // $zero );
	    }
	}
    }
    my %add = (
	'TRESORERIE' => qr/^5:.$/,
	'AVANCES' => qr/^1:.$/,
	'CREANCES' => undef,
	'DETTES' => undef,
	);
    my %addentry = (
	'TRESORERIE' => qr/^5:.:.$/,
	'AVANCES' => qr/^1:.:.$/,
	'CREANCES' => undef,
	'DETTES' => undef,
	);
    for my $part (@a1parts) {
	my $val=
	$partlines->{$part} = $self->listaccounts(
	    'base' => [
		keys %{$balances->{'cur'}->{$part}->{'accounts'}},
		keys %{$balances->{'prev'}->{$part}->{'accounts'}}
	    ],
	    'remove' => qr/^.$/,
	    'add' => $add{$part},
	    'add-entry' => $addentry{$part},
	    'accounts' => $accounts,
	    )
    }
    $res .= $self->latexAnnexeBody(
	'values' => $balances,
	'partlines' => $partlines,
	'parts' => [ 'TRESORERIE', 'AVANCES' ],
	'factors' => {
	    'TRESORERIE' => -1,
	},
	'columns' => [ 'prev', 'cur' ],
	'prefix' => 'annA',
	'secname' => 'st',
	);
    $res .= $self->latexAnnexeBody(
	'values' => $balances,
	'partlines' => $partlines,
	'parts' => [ 'CREANCES', 'DETTES' ],
	'factors' => {
	    'CREANCES' => -1,
	},
	'columns' => [ 'prev', 'cur' ],
	'prefix' => 'annA',
	'secname' => 'cd',
	'part-offset' => 2,
	);
    $res .= $self->latexAnnexeTotal(
	'parts' => [ 'COL1', 'COL2' ],
	'factors' => {
	    'COL1' => -1,
	},
	'columns' => [ 'prev', 'cur' ],
	'prefix' => 'annA',
	'secname' => 'tot',
	'total' => $total,
	'part-offset' => 4,
	);

    @a1parts = ('ATTENTE', 'COPROP');
    $balances=undef;
    {
	my $bals=Copro::Compta::Balances::get(
	    $config,
	    $self->compta,
	    'mid-args' => Copro::Compta::Balances::getParamsForAnnexe('annexe1-next'),
	    'end-args' => [ '--flat' ],
	    'ensure-balances' => \@a1parts,
	    'ledger-kind' => [ 'journal' ],
	    'exercices' => [ $exercice ],
	    );
	foreach my $part (@a1parts) {
	    foreach my $account (keys %{$bals->{$part}->{'accounts'}}) {
		my $npart = 'CREDIT';
		my $val = $bals->{$part}->{'accounts'}->{$account};
		if ($val->is_neg) {
		    $npart= 'DEBIT';
		}
		$balances->{$npart}->{$part}->{'accounts'}->{$account}=$val;
		$balances->{$npart}->{$part}->{'total'} += $val;
	    }
	}
    }
    foreach my $part (@a1parts) {
	$partlines->{$part} = $self->listaccounts(
	    'base' => [
		keys %{$balances->{'DEBIT'}->{$part}->{'accounts'}},
		keys %{$balances->{'CREDIT'}->{$part}->{'accounts'}}
	    ],
	    'remove' => qr/^(.(:.)?|4:5:0)?$/,
	    'accounts' => $accounts,
	    )
    }
    #print Dumper($balances);
    $res .= $self->latexAnnexeBody(
	'values' => $balances,
	'partlines' => $partlines,
	'parts' => [ 'COPROP' ],
	'factors' => {
	    'DEBIT' => -1,
	},
	'columns' => [ 'DEBIT', 'CREDIT' ],
	'prefix' => 'annAcop',
	'secname' => '',
	);
    $res .= $self->latexAnnexeBody(
	'values' => $balances,
	'partlines' => $partlines,
	'parts' => [ 'ATTENTE' ],
	'factors' => {
	    'DEBIT' => -1,
	},
	'columns' => [ 'DEBIT', 'CREDIT' ],
	'prefix' => 'annAattente',
	'secname' => '',
	);
    #$balances->{$year}=$self->normalizeBalances($bals);
    return $res;
}

sub annexe2 {
    my $self=shift;
    my $config=shift;
    my $exercice=shift;

    my $res;
    
    my $balances;
    my @a2parts=('CH-COUR', 'PR-COUR', 'CH-TROPEX', 'PR-TROPEX');

    my %years = (
	'ex-prev' => $exercice->prev,
	'ex-cur' => $exercice,
	);
    for my $year ('ex-prev', 'ex-cur') {
	my $bals=Copro::Compta::Balances::get(
	    $config,
	    $self->compta,
	    'mid-args' => Copro::Compta::Balances::getParamsForAnnexe('annexe2'),
	    'end-args' => [ '--flat' ],
	    'ensure-balances' => \@a2parts,
	    'ledger-kind' => [ 'journal' ],
	    'exercices' => [ $years{$year} ],
	    );

	$balances->{$year}=$self->normalizeBalances($bals);
    }
    for my $budget ('cur', 'next', 'next-next') {
	my $bals=Copro::Compta::Balances::get(
	    $config,
	    $self->compta,
	    'mid-args' => Copro::Compta::Balances::getParamsForAnnexe('annexe2'),
	    'end-args' => [ '--flat' ],
	    'ensure-balances' => \@a2parts,
	    'exercices' => [ $exercice ],
	    'ledger-kind' => [ 'budget', $budget.'-year' ],
	    );

	$balances->{'bud-'.$budget}=$self->normalizeBalances($bals);
    }
    my $accounts=$self->loadAccountsFromConfig($config);
    my $partlines;
    my $columns= [
	'ex-prev', 'bud-cur', 'ex-cur',
	'bud-next', 'bud-next-next',
	];
    for my $part (@a2parts) {
	my $val=
	$partlines->{$part} = $self->listaccounts(
	    'base' => [
		map {
		    keys %{$balances->{$_}->{$part}->{'accounts'}}
		} (keys %{$balances}),
	    ],
	    'remove' => qr/^.$/,
	    'accounts' => $accounts,
	    )
    }
    $res .= $self->latexAnnexeBody(
	'values' => $balances,
	'partlines' => $partlines,
	'parts' => [ 'CH-COUR', 'PR-COUR' ],
	'factors' => {
	    'CH-COUR' => -1,
	},
	'columns' => $columns,
	'prefix' => 'annBcour',
	'secname' => '',
	'print-total' => 0,
	);
    $res .= $self->latexAnnexeBody(
	'values' => $balances,
	'partlines' => $partlines,
	'parts' => [ 'CH-TROPEX', 'PR-TROPEX' ],
	'factors' => {
	    'CH-TROPEX' => -1,
	},
	'columns' => $columns,
	'prefix' => 'annBtropex',
	'secname' => '',
	'print-total' => 0,
	);

    my $totbals;
    #print STDERR Dumper($balances);
    foreach my $col (keys %{$balances}) {
	foreach my $part (keys %{$balances->{$col}}) {
	    my $val=$balances->{$col}->{$part}->{'total'};
	    if (defined($val)) {
		$totbals->{$col}->{$part}->{'accounts'}->{'a'} += $val;
		$totbals->{$col}->{$part}->{'accounts'}->{'c'} += $val;
	    }
	}
    }
    foreach my $col ('ex-prev', 'bud-cur', 'ex-cur') {
	foreach my $pair (['CH-COUR', 'PR-COUR' ], [ 'CH-TROPEX', 'PR-TROPEX' ]) {
	    my $ch=$pair->[0];
	    my $pr=$pair->[1];
	    my $diff = Math::BigRat->new(0) +
		($balances->{$col}->{$ch}->{'total'} // 0) +
		($balances->{$col}->{$pr}->{'total'} // 0);
	    if ($diff->is_neg) {
		$totbals->{$col}->{$pr}->{'accounts'}->{'b'} -= $diff;
		$totbals->{$col}->{$pr}->{'accounts'}->{'c'} -= $diff;
	    } elsif (! $diff->is_zero) {
		$totbals->{$col}->{$ch}->{'accounts'}->{'b'} -= $diff;
		$totbals->{$col}->{$ch}->{'accounts'}->{'c'} -= $diff;
	    }
	}
    }
    my $totpartlines;
    {
	my $lines = {
	    'a' => {
		'type' => 'sstotal',
	    },
	    'b' => {
		'type' => 'solde',
	    },
	    'c' => {
		'type' => 'somme',
	    },
	};
	foreach my $part ('CH-COUR', 'PR-COUR', 'CH-TROPEX', 'PR-TROPEX') {
	    $totpartlines->{$part}=$lines;
	}
    };
    $res .= $self->latexAnnexeBody(
	'values' => $totbals,
	'partlines' => $totpartlines,
	'parts' => [ 'CH-COUR', 'PR-COUR' ],
	'factors' => {
	    'CH-COUR' => -1,
	},
	'columns' => $columns,
	'prefix' => 'annBcourTot',
	'secname' => '',
	'print-total' => 0,
	);
    $res .= $self->latexAnnexeBody(
	'values' => $totbals,
	'partlines' => $totpartlines,
	'parts' => [ 'CH-TROPEX', 'PR-TROPEX' ],
	'factors' => {
	    'CH-TROPEX' => -1,
	},
	'columns' => $columns,
	'prefix' => 'annBtropexTot',
	'secname' => '',
	'print-total' => 0,
	);
    return $res;
}

sub annexe3 {
    my $self=shift;
    my $config=shift;
    my $exercice=shift;
    my $desc=shift;

    my $res;
    
    my $balances;

    my %years = (
	'ex-prev' => $exercice->prev,
	'ex-cur' => $exercice,
	);
    for my $year ('ex-prev', 'ex-cur') {
	my $bals=Copro::Compta::Balances::get(
	    $config,
	    $self->compta,
	    'mid-args' => Copro::Compta::Balances::getParamsForAnnexe('annexe3'),
	    'end-args' => [ '--flat' ],
	    'ensure-balances' => [],
	    'ledger-kind' => [ 'journal' ],
	    'exercices' => [ $years{$year} ],
	    );

	$balances->{$year}=$self->normalizeBalances($bals);
    }
    for my $budget ('cur', 'next', 'next-next') {
	my $bals=Copro::Compta::Balances::get(
	    $config,
	    $self->compta,
	    'mid-args' => Copro::Compta::Balances::getParamsForAnnexe('annexe3'),
	    'end-args' => [ '--flat' ],
	    'ensure-balances' => [],
	    'exercices' => [ $exercice ],
	    'ledger-kind' => [ 'budget', $budget.'-year' ],
	    );

	$balances->{'bud-'.$budget}=$self->normalizeBalances($bals);
    }
    my $accounts=$self->loadAccountsFromConfig($config);
    my $bals;
    my $partlines;
    my $columns= [
	'ex-prev', 'bud-cur', 'ex-cur',
	'bud-next', 'bud-next-next',
	];
    
    my $ras;
    my $zero=Math::BigRat->new(0);
    #print STDERR Dumper($balances);
    foreach my $col (keys %{$balances}) {
	foreach my $part (keys %{$balances->{$col}}) {
	    if ($part !~ m,^(NET|RA)/(.*)$,) {
		carp "Strange group '$part' in annexe 3";
	    }
	    my $subpart=$1;
	    my $key=$2;
	    my $keyinfo=$desc->info('keys', 'K.Id', $key);
	    my $keynum="2|".$keyinfo->elm(0, 'K.num');
	    my $subtype = 'net';
	    my $ksubpart = '3-net';
	    if ($subpart eq 'RA') {
		$subtype = 'ra';
		$ksubpart = '2-ra';
	    }
	    $partlines->{'ALL'}->{$keynum."|0"} = {
		'type' => 'key',
		'libelle' => $keyinfo->elm(0, 'K.Label'),
		'poste' => $key,
	    };
	    $partlines->{'ALL'}->{$keynum."|3"} = {
		'type' => 'sum',
		'libelle' => $keyinfo->elm(0, 'K.Label'),
		'poste' => $key,
	    };
	    foreach my $account (keys %{$balances->{$col}->{$part}->{'accounts'}}) {
		my $val=$balances->{$col}->{$part}->{'accounts'}->{$account};
		my $acc = $accounts->byName($account);
		my $keyacc = $keynum."|2|".$account;
		$bals->{$col}->{'ALL'}->{'accounts'}
		->{$keyacc."|".$ksubpart}=$val;
		$ras->{$keyacc."|".$ksubpart} = {
		    'ra' => $keyacc."|2-ra",
		    'net' => $keyacc."|3-net",
		    'ch' => $keyacc."|1-ch",
		};

		$bals->{$col}->{'ALL'}->{'accounts'}
		->{$keyacc."|1-ch"}+=$val;
		if ($subpart eq 'RA') {
		    $bals->{$col}->{'ALL'}->{'accounts'}
		    ->{$keyacc."|3-net"} += $zero;		    
		} else {
		    $bals->{$col}->{'ALL'}->{'accounts'}
		    ->{$keynum."|3"} += $val;
		    $bals->{$col}->{'ALL'}->{'accounts'}
		    ->{"3"} += $val;
		}
		$partlines->{'ALL'}->{$keyacc."|0-label"} = {
		    'type' => 'label',
		    'libelle' => $acc->note,
		    'poste' => $self->accountname($acc->name),
		};
		$partlines->{'ALL'}->{$keyacc."|".$ksubpart} = {
		    'type' => $subtype,
		    'libelle' => $acc->note,
		    'poste' => $self->accountname($acc->name),
		};
		$partlines->{'ALL'}->{$keyacc."|1-ch"} = {
		    'type' => 'ch',
		    'libelle' => $acc->note,
		    'poste' => $self->accountname($acc->name),
		};
		$partlines->{'ALL'}->{$keyacc."|3-net"} = {
		    'type' => 'net',
		    'libelle' => $acc->note,
		    'poste' => $self->accountname($acc->name),
		};
		$partlines->{'ALL'}->{"3"} = {
		    'type' => 'sumtot',
		    'libelle' => 'Charges courantes nettes',
		    'poste' => $key,
		};
		$bals->{$col}->{'ALL'}->{'total'} += $val;		
	    }
	}
    }
    foreach my $ra (values %{$ras}) {
	next if exists($ras->{$ra->{'ra'}});
	$partlines->{'ALL'}->{$ra->{'ch'}}->{'type'}='chnet';
	delete($partlines->{'ALL'}->{$ra->{'net'}});
    }
    
    $res .= $self->latexAnnexeBody(
	'values' => $bals,
	'partlines' => $partlines,
	'parts' => [ 'ALL' ],
	'factors' => {
	    'ALL' => -1,
	},
	'columns' => $columns,
	'prefix' => 'annC',
	'secname' => '',
	'print-total' => 0,
	);
    return $res;
}

sub annexe4 {
    my $self=shift;
    my $config=shift;
    my $exercice=shift;
    my $desc=shift;

    my $res;
    
    my $balances;

    {
	my $bals=Copro::Compta::Balances::get(
	    $config,
	    $self->compta,
	    'mid-args' => Copro::Compta::Balances::getParamsForAnnexe('annexe4'),
	    'end-args' => [ '--flat' ],
	    'ensure-balances' => [],
	    'ledger-kind' => [ 'journal' ],
	    'exercices' => [ $exercice ],
	    );

	$balances->{'cur-ex'}=$bals;#$self->normalizeBalances($bals);
    }
    {
	my $bals=Copro::Compta::Balances::get(
	    $config,
	    $self->compta,
	    'mid-args' => Copro::Compta::Balances::getParamsForAnnexe('annexe4'),
	    'end-args' => [ '--flat' ],
	    'ensure-balances' => [],
	    'exercices' => [ $exercice ],
	    'ledger-kind' => [ 'budget', 'tropex', 'done' ],
	    );
	#print STDERR Dumper($bals);
	$balances->{'budgets'}=$bals;#$self->normalizeBalances($bals);
    }
    my $accounts=$self->loadAccountsFromConfig($config);
    my $bals;
    my $partlines = {
	'TOTAL' => {
	    '3' => {
		'type' => 'full',
		'libelle' => 'Total des tropex',
	    },
	},
    };
    my $columns= [
	'budgets', 'depenses', 'provisions', 'solde',
	];
    
    my $ras;
    my $keys;
    my $zero=Math::BigRat->new(0);
    #print STDERR Dumper($balances);

    foreach my $tropex ($exercice->tropexDone) {
	$partlines->{$tropex->type.""}->{"2|".$tropex->tag."|0"} = {
	    'type' => 'op',
	    'libelle' => $tropex->label,
	    'poste' => $tropex->tag,
	};
    }

    foreach my $col (keys %{$balances}) {
	foreach my $part (keys %{$balances->{$col}}) {
	    if ($part !~ m,^(CH|PR)/(CHEXCEPT|CHTRAVAUX)/([^/]*)/(NET|RA)/(.*)$,) {
		carp "Strange group '$part' in annexe 3";
	    }
	    my $chpr=$1;
	    my $group=$2;
	    my $tropex=$3;
	    my $subpart=$4;
	    my $key=$5;
	    my $keyinfo=$desc->info('keys', 'K.Id', $key);
	    my $tropexprefix="2|".$tropex;
	    my $keynum=$tropexprefix."|2|".$keyinfo->elm(0, 'K.num');
	    my $subtype = 'net';
	    my $ksubpart = '3-net';
	    if ($subpart eq 'RA') {
		$subtype = 'ra';
		$ksubpart = '2-ra';
	    }
	    my $dcol=$col;
	    if ($col ne "budgets") {
		$dcol = ($chpr eq "CH") ? "depenses" : "provisions";
	    }
	    if (! exists($partlines->{$group}->{$tropexprefix."|0"})) {
		carp("Tropex $group/$tropex does not exists!\n");
	    }
	    if ($chpr eq "CH") {
		$partlines->{$group}->{$keynum."|0"} = {
		    'type' => 'key',
		    'libelle' => $keyinfo->elm(0, 'K.Label'),
		    'poste' => $key,
		};
	    }
	    $partlines->{$group}->{$keynum."|3"} = {
		'type' => 'sum',
		'libelle' => $keyinfo->elm(0, 'K.Label'),
		'poste' => $key,
	    };
#	    $partlines->{$group}->{$tropexprefix."|3"} = {
#		'type' => 'sumtot',
#		'libelle' => $tropex,
#		'poste' => $tropex,
#	    };
	    $keys->{$group}->{$tropexprefix}->{$key}=$keynum."|3";
	    my @sumcols = ();
	    if ($dcol eq "budgets") {
		push @sumcols, $dcol if $chpr eq "CH";
	    } else {
		push @sumcols, $dcol, "solde";
	    }
	    foreach my $account (keys %{$balances->{$col}->{$part}->{'accounts'}}) {
		my $val=$balances->{$col}->{$part}->{'accounts'}->{$account};
		my $acc = $accounts->byName($account);
		my $keyacc = $keynum."|2|".$account;
		
		$bals->{$dcol}->{$group}->{'accounts'}
		->{$keyacc."|".$ksubpart}=$val;

		$bals->{$dcol}->{$group}->{'accounts'}
		->{$keyacc."|1-ch"}+=$val;
		if ($subpart eq 'RA') {
		    for my $lcol (@sumcols) {
			$bals->{$lcol}->{$group}->{'accounts'}
			->{$tropexprefix."|3"} += $zero;
			$bals->{$lcol}->{$group}->{'accounts'}
			->{$keynum."|3"} += $zero;
			$bals->{$lcol}->{$group}->{'accounts'}
			->{"3"} += $zero;
			$bals->{$lcol}->{'TOTAL'}->{'accounts'}
			->{"3"} += $zero;
		    }
		    $bals->{$dcol}->{$group}->{'accounts'}
		    ->{$keyacc."|3-net"} += $zero;		    
		} else {
		    for my $lcol (@sumcols) {
			$bals->{$lcol}->{$group}->{'accounts'}
			->{$tropexprefix."|3"} += $val;
			$bals->{$lcol}->{$group}->{'accounts'}
			->{$keynum."|3"} += $val ;
			$bals->{$lcol}->{$group}->{'accounts'}
			->{"3"} += $val;
			$bals->{$lcol}->{'TOTAL'}->{'accounts'}
			->{"3"} += $val;
		    }
		}
		if ($chpr eq "CH") {
		    $ras->{$group}->{$keyacc."|".$ksubpart} = {
			'ra' => $keyacc."|2-ra",
			'net' => $keyacc."|3-net",
			'ch' => $keyacc."|1-ch",
		    };
		    $partlines->{$group}->{$keyacc."|0-label"} = {
			'type' => 'label',
			'libelle' => $acc->note,
			'poste' => $self->accountname($acc->name),
		    };
		    $partlines->{$group}->{$keyacc."|".$ksubpart} = {
			'type' => $subtype,
			'libelle' => $acc->note,
			'poste' => $self->accountname($acc->name),
		    };
		    $partlines->{$group}->{$keyacc."|1-ch"} = {
			'type' => 'ch',
			'libelle' => $acc->note,
			'poste' => $self->accountname($acc->name),
		    };
		    $partlines->{$group}->{$keyacc."|3-net"} = {
			'type' => 'net',
			'libelle' => $acc->note,
			'poste' => $self->accountname($acc->name),
		    };
		}
		$partlines->{$group}->{"3"} = {
		    'type' => 'sumtot',
		    'libelle' => 'Charges nettes',
		    'poste' => $key,
		};
		$bals->{$dcol}->{$group}->{'total'} += $val;		
	    }
	}
    }
    foreach my $group (keys %{$ras}) {
	foreach my $ra (values %{$ras->{$group}}) {
	    next if exists($ras->{$group}->{$ra->{'ra'}});
	    $partlines->{$group}->{$ra->{'ch'}}->{'type'}='chnet';
	    delete($partlines->{$group}->{$ra->{'net'}});
	}
    }
    #print STDERR Dumper($keys);
    foreach my $group (keys %{$keys}) {
	foreach my $tropex (values %{$keys->{$group}}) {
	    if (scalar(keys(%{$tropex})) == 1) {
		delete($partlines->{$group}->{(values %{$tropex})[0]});
	    }
	}
    }
    $res .= $self->latexAnnexeBody(
	'values' => $bals,
	'partlines' => $partlines,
	'parts' => [ 'CHTRAVAUX' ],
	'factors' => {
	    'budgets' => -1,
	    'depenses' => -1,
	},
	'columns' => $columns,
	'prefix' => 'annD',
	'secname' => 'travaux',
	'print-total' => 0,
	);
    $res .= $self->latexAnnexeBody(
	'values' => $bals,
	'partlines' => $partlines,
	'parts' => [ 'CHEXCEPT' ],
	'factors' => {
	    'budgets' => -1,
	    'depenses' => -1,
	},
	'columns' => $columns,
	'prefix' => 'annD',
	'secname' => 'opex',
	'print-total' => 0,
	);
    $res .= $self->latexAnnexeBody(
	'values' => $bals,
	'partlines' => $partlines,
	'parts' => [ 'TOTAL' ],
	'factors' => {
	    'budgets' => -1,
	    'depenses' => -1,
	},
	'columns' => $columns,
	'prefix' => 'annD',
	'secname' => 'full',
	'print-total' => 0,
	);
    return $res;
}

sub annexe5 {
    my $self=shift;
    my $config=shift;
    my $exercice=shift;
    my $desc=shift;

    my $res='\newcommand{\annEbody}{}'."\n";

    my $partlines;

    my %tropexes;
    my %exercices = (
	$exercice->start -> $exercice,
	);
    my $old_exercice=$exercice;
    
    foreach my $tropex ($exercice->tropexOpened) {
	print STDERR "TODO: ".$tropex->label."\n";
	$tropexes{$tropex->tag}=$tropex;
	my $ex = $tropex->start;
	while ($ex->start < $old_exercice->start) {
	    $old_exercice = $old_exercice->prev;
	    die "Missing previous exercice" if not defined($old_exercice);
	    $exercices{$old_exercice->start} = $old_exercice;
	}
	
    }
    
    return $res;
}

sub run($$) {
    my $self=shift;
    my $config=shift;
    my $args=$config->internal->get('args');
    my $descdir;

    my $exercice;
    {
	my @exercices=$config->default->exercices;
	if (scalar(@exercices) != 1) {
	    die "Only one exercice can be specified";
	}
	$exercice=$exercices[0];
    }

    my $p = new Getopt::Long::Parser
	config => ['require_order'];
    my $global_options=[
	'copro-description-dir=s' => \$descdir,
	];
    if (not defined($descdir)) {
	$descdir='archives/Copro';
    }
    my $desc=$self->loadCoproDesc($descdir, $exercice->id."-end");
    
    binmode(STDOUT, ":utf8");
    print $self->annexe1($config, $exercice);
    print $self->annexe2($config, $exercice);
    print $self->annexe3($config, $exercice, $desc);
    print $self->annexe4($config, $exercice, $desc);
    #print $self->annexe5($config, $exercice, $desc);
    
}

1;
#	 '--group-by', '("CLOTURE" if (tag("PHASE")+"")=="CLOTURE")+"/"+tag("REPTYPECH")+":"+tag("REPTROPEX")+"/"+tag("REPCLEF")+"/"+tag("REPRA")',
