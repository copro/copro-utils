package Copro::Compta::CMD::AppelManuel;
use Moose;
use namespace::sweep;
use Math::BigRat;

with (
    'Copro::Compta::Roles::CMD',
    'Copro::Utils::Roles::Date',
    'Copro::Compta::Roles::Appel',
    );

use Data::Dumper;
$Data::Dumper::Useperl = 1;

sub synopsis {
    return "génère un appel de fonds depuis des clés et des sommes";
}

my $date;
my $descdir;
# TODO get default key
my $curkey='GEN:A+B';
my $amounts={};
my $part=Math::BigRat->new(1);
sub options {
    my $self = shift;
    return [
	'date=s' => sub {
	    my $optname=shift;
	    my $value=shift;
	    # TODO: check date
	    $date=$value;
	},
	'copro-description-dir=s' => \$descdir,
	'clef=s' => \$curkey,
	'amount=s' => sub {
	    my $optname=shift;
	    my $value=shift;
	    # TODO: check date
	    $amounts->{$curkey} += Math::BigRat->new($value);
	},
	];
}

sub run($$) {
    my $self=shift;
    my $config=shift;
    my $args=$config->internal->get('args');
    
    #my $exercice=$config->section("default")->exercice;
    shift @ARGV;
    
    my $p = new Getopt::Long::Parser
	config => ['require_order'];
    my $global_options=$self->options();
    
    $p->getoptions(@{$global_options});

    $date ||= $self->today;
    if (not defined($descdir)) {
	$descdir='/home/vdanjean/travail/perso/immobilier/syndic/dossiers/tantiemes/tantiemes';
    }
    my $desc=$self->loadCoproDesc($descdir);
    my $appel=$self->appelCompute(
	'copro' => $desc,
	'amounts' => $amounts,
	);

    my $str=$self->appel2string($appel, $date);
    binmode( STDOUT, ":utf8");
    print $str, "\n";

    #print "RUN: '", join("' '", @ledgerargs), "'\n";
    #exec @ledgerargs or die "Cannot execute ledger\n";
}

1;
