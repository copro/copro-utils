package Copro::Compta::CMD::Genpc;
use Moose;
use namespace::sweep;
use Math::BigRat;
use Copro::Compta::Accounts;

with (
    'Copro::Compta::Roles::CMD',
    'Copro::Compta::Roles::Account',
    );

sub synopsis {
    return "genère le CSV des postes comptables"
}

sub options {
    return [];
}

sub run($$) {
    my $self=shift;
    my $config=shift;
    my $args=$config->internal->get('args');
    my $date;
    my $descdir;
    # TODO get default key
    my $curkey='GEN:A+B';
    my $amounts={};

    my $accounts = $self->loadAccountsFromConfig($config);

    binmode(STDOUT, ":utf8");
    print "poste;libelle;parent;type\n";
    foreach my $account (sort { $a->name cmp $b->name } $accounts->accounts) {
	print $self->accountname($account->name), ";";
	print (($account->note//''), ";");
	my $parent = $account->parent;
	if (defined($parent)) {
	    print $self->accountname($account->parent->name), ";";
	} else {
	    print ";";
	}
	print "\n";
    }

}

1;
