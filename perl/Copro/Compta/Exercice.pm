package Copro::Compta::Exercice;
use Moose;
use namespace::autoclean;
use Path::Class::Dir;
use Copro::Compta::Ledger;

with qw(Copro::Compta::Roles::ComptaAttr);

has 'label' => (
    is  => 'ro',
    isa => 'Str',
    required => 1,
    );

has 'id' => (
    is  => 'ro',
    isa => 'Str',
    required => 1,
    );

has 'date_start' => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    required => 1,
    init_arg => 'start',
    default => sub {
	shift->id."-01-01"
    },
    );

has 'date_end' => (
    is => 'ro',
    isa => 'Str',
    lazy => 1,
    required => 1,
    init_arg => 'end',
    default => sub {
	shift->id."-12-31"
    },
    );

has 'path' => (
    is  => 'ro',
    isa => 'Path::Class::Dir',
    lazy => 1,
    required => 1,
    builder => '_build_path',
    writer => '_set_path',
    init_arg => undef,
    );

has 'relpath' => (
    is  => 'ro',
    isa => 'Path::Class::Dir',
    lazy => 1,
    coerce   => 1,
    required => 1,
    builder => '_build_relpath',
    writer => '_set_relpath',
    init_arg => 'path',
    trigger => \&_relpath_set,
    );

has 'prev' => (
    is    => 'ro',
    isa   => 'Maybe[Copro::Compta::Exercice]',
    );

has 'next' => (
    is    => 'ro',
    writer => '_set_next',
    isa   => 'Maybe[Copro::Compta::Exercice]',
    );

sub tropexAll {
    my $self = shift;
    my $compta = $self->compta;
    my $start = $self->start;
    
    my @tropex = grep {
	$_->start->start <= $start
	    && ( (! $_->done) || ($start <= $_->end->start)) 
    } $self->compta->allTropex;
}

sub tropexDone {
    my $self = shift;
    my $compta = $self->compta;
    my $start = $self->start;
    
    my @tropex = grep {
	$_->start->start <= $start
	    && $_->done
	    && $_->end->start == $start
    } $self->compta->allTropex;
}

sub tropexOpened {
    my $self = shift;
    my $compta = $self->compta;
    my $start = $self->start;
    
    my @tropex = grep {
	$_->start->start <= $start
	    && (! $_->done
		|| $start < $_->end->start)
    } $self->compta->allTropex;
}

sub _build_path {
    my $self = shift;
    my $path=Path::Class::Dir->new($self->compta->path, $self->relpath);
    return $path;
}

sub _build_relpath {
    my $self = shift;
    return $self->id;
}

sub _relpath_set {
    my ( $self, $relpath, $old_relpath ) = @_;

    my $path=Path::Class::Dir->new($relpath);
    if (not $path->is_relative) {
	die "Setting path with a non relative path '".$path.
	    "' in Exercice '".$self->id."'\n";
    }
    $self->_set_path(Path::Class::Dir->new($self->compta->path, $path))
}

has '_ledgers' => (
    traits    => ['Array'],
    is        => 'ro',
    isa       => 'ArrayRef[Copro::Compta::Ledger]',
    default   => sub { [] },
    handles   => {
	_add_ledger    => 'push',
	count          => 'count',
	ledgers        => 'elements',
    },
    );

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;
    
    if ( scalar(@_) == 1 ) {
	return $class->$orig(
	    'compta' => $_[0],
	    );
    }
    else {
	my %hash = ( @_ );
	# use default data_start and date_end if none specified
	foreach my $v ('start', 'end') {
	    if (exists($hash{$v}) && ($hash{$v}//'') eq '') {
		delete($hash{$v});
	    }
	}
	return $class->$orig(%hash);
    }
};

sub BUILD {
    my $self = shift;
    if ($self->date_end le $self->date_start) {
	die "Exercice ".$self->id." start after its end! (".
	    $self->date_start."/".$self->date_end.")\n";
    }
    if (defined($self->prev)) {
	if ($self->prev->date_start ge $self->date_end) {
	    die "Exercice ".$self->id." start after previous exercice ".
		$self->prev->id."!\n";
	}
	$self->prev->_set_next($self);
    }
}

sub addledger {
    my $self=shift;
    my %params=@_;

    my $ledger=Copro::Compta::Ledger->new(
	@_,
	'kind' => Copro::Compta::Ledger::Str2kind($params{subtype}),
	'exercice' => $self,
	);

    $self->_add_ledger($ledger);
    return $ledger;
}
use IO::Handle;

has '_loaded' => (
    is       => 'rw',
    isa      => 'Bool',
    required => 1,
    lazy     => 1,
    default  => 0,
    );

sub load {
    my $self = shift;

    if ($self->_loaded) {
	#print STDERR "Skipping reloading of exercice ".$self->label."\n";
	#flush STDERR;
	return;
    }
    print STDERR "Loading exercice ".$self->label."...";
    flush STDERR;
    map { $_->journal ; } $self->ledgers;
    print STDERR "done\n";
    $self->_loaded(1);
}

__PACKAGE__->meta->make_immutable;

1;
