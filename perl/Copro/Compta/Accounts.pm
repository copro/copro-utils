package Copro::Compta::Accounts;
use Moose;
use namespace::sweep;
use Carp;

has '_accounts' => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef[Copro::Compta::Account]',
    default   => sub { {} },
    handles   => {
	_register      => 'set',
	_get           => 'get',
	byName         => 'get',
	accounts       => 'values',
	accountNames   => 'keys',
	#count          => 'count',
	#option_pairs   => 'kv',
	_exists        => 'exists',
	isAccount      => 'exists',
    },
    );

has '_aliases' => (
    traits    => ['Hash'],
    is        => 'ro',
    isa       => 'HashRef[Copro::Compta::Account]',
    default   => sub { {} },
    handles   => {
	_registerAlias => 'set',
	_getAlias      => 'get',
	byAlias        => 'get',
	accountAliases => 'keys',
	#count          => '_count',
	#option_pairs   => '_kv',
	_existsAlias   => 'exists',
	isAlias        => 'exists',
    },
    );

sub get {
    my $self = shift;
    my $name = shift;
    return $self->byName($name) // $self->byAlias($name);
}

sub _removeAlias {
    my $self = shift;
    my $alias = shift;

    return if not $self->isAlias($alias);
    if ($self->_getAlias($alias)->hasAlias($alias)) {
	carp "Alias '$alias' is still an alias of its '".
	    $self->_getAlias($alias)->name."' account";
    }
    delete($self->_aliases->{$alias});
}

sub register {
    my $self = shift;
    my $name = shift;
    my $account;

    if ($self->_exists($name)) {
	$account=$self->_get($name);
    } else {
	$account=Copro::Compta::Account->new(
	    'accounts' => $self,
	    'name' => $name,
	    );
	$self->_register($account->name, $account);
    }
    if ($self->_existsAlias($name)) {
	carp "Registering the '$name' account whereas an alias already exists";
    }
	
    return $account;
}

sub _registerAliases {
    my $self = shift;
    my $account = shift;

    foreach my $alias ($account->aliases) {
	if ($self->_existsAlias($alias)) {
	    my $accountAlias=$self->_getAlias($alias);
	    next if $accountAlias == $account;
	    carp "Account alias '$alias' point to '".
		$account->name."' (previously '".
		$accountAlias->name."')";
	    $accountAlias->removeAlias($alias);
	}
	$self->_registerAlias($alias, $account);
    }
    return $account;
}

sub exists {
    my $self = shift;
    my $name = shift;

    return $self->isAccount($name) || $self->isAlias($name);
}

__PACKAGE__->meta->make_immutable;

1;
