package Copro::Compta::Roles::ComptaAttr;
use Moose::Role;
#use namespace::autoclean;

has '_compta' => (
    reader => 'compta',
    is => 'ro',
    isa => 'Copro::Compta',
    required => 1,
    weak_ref => 1,
    init_arg => 'compta',
    traits  => [ 'NoGetopt' ], 
    );

1;
