package Copro::Compta::Roles::ReadJournals;
use Moose::Role;
use namespace::sweep;

sub load_journals() {
    my $self = shift;
    my $config = shift;
    my $options = shift // {};
    my @exercices= @{$options->{'exercices'} //
			 [ $config->section("default")->exercices ]};

    @exercices=sort {
	$a->id cmp $b->id
    } @exercices;

    if ($options->{'set-dstart'} // 0) {
	if (! $self->has_dstart) {
	    $self->dstart($exercices[0]->date_start);
	}
    }
    if ($options->{'set-dend'} // 0) {
	if (! $self->has_dend) {
	    $self->dend($exercices[-1]->date_end);
	}
    }
    
    if ($options->{'load-prev-exercice'} // 0) {
	my $nb=$options->{'load-prev-exercice'};
	while($nb--) {
	    unshift @exercices, $exercices[0]->prev;
	}
    }
    
    my $journal_exercice="";
    foreach my $ledger (
	$self->compta->select_ledgers(
	    $config,
	    'exercices' => \@exercices,
	    'kind' => ['pc'],
	    #'load' => 1,
	),
	) {
	$journal_exercice .= "include ".$ledger->path."\n";
    }
    foreach my $ledger (
	$self->compta->select_ledgers(
	    $config,
	    'exercices' => \@exercices,
	    'kind' => ['journal'],
	    #'load' => 1,
	),
	) {
	$journal_exercice .= "include ".$ledger->path."\n";
    }
    #print $journal_exercice."\n";
    #print STDERR $journal_exercice, "\n";
    #binmode( STDOUT, ":utf8");
    print STDERR "Reading journals (".join(',',map {$_->id} @exercices).")...";
    my $journal=Copro::Compta::Ledger->_parser->read_string($journal_exercice);
    print STDERR "done\n";
    
    return $journal;
}

1;
