package Copro::Compta::Roles::ExtraitCompte;
use Moose::Role;
use namespace::sweep;

use Copro::Utils::Amount qw(val_str);
use Copro::Utils::LaTeX  qw(latexize hash2options);
use Copro::Utils::Ledger qw(set_amount set_rcdate set_knowndate
 journal_date_minmax);

sub LaTeXExtraitCompte {
    my $self = shift;
    my $options = { @_ };
    my $journal = $options->{'journal'} // die "E: journal missing\n";
    my $accounts = $options->{'accounts'} // die "E: accounts missing\n";
    my $dstart = $options->{'date-start'} // die "No date-start for 'ExtraitCompte'\n";
    my $dend = $options->{'date-end'} // die "No date-end for 'ExtraitCompte'\n";
    my $dlimit = $options->{'date-limit'}; # Limite des opérations connues
    my $str;

    my $iter_p=$journal->iterator(
	'follow-includes'=>1,
	'select-element' => sub {
            return shift->isa("Ledger::Posting");
        });

    my $restr=$options->{'accounts-regexp'} // '^4:5:0';
    my $re=qr /$restr/;
    #print Dumper($accounts);
    my $pp;
    while (defined(my $p=$iter_p->next)) {
	my $account=$accounts->get($p->account->name);
	if (not defined($account)) {
	    die "Invalid account name ".$p->account->name;
	}
	my $n=$account->name;
	next if ($n !~ /$re/);
	push @{$pp->{$n}}, $p;
	set_rcdate($p->parent);
	set_knowndate($p->parent);
	set_amount($p);
	if ($p->parent->var('rcdate_str') lt $dstart) {
	    $p->var('keep', 0);
	    next;
	}
	if ($p->parent->var('rcdate_str') gt $dend) {
	    $p->var('keep', 0);
	    next;
	}
	if (defined($dlimit) && $p->parent->var('knowndate_str') gt $dlimit) {
	    $p->var('keep', 0);
	    next;
	}
	my $phase=$p->tag('PHASE')//'';
	if ($phase =~ /OUVERTURE|CLOTURE/) {
	    $p->var('keep', 0);
	    next;
	}
	$p->var('keep', 1);
    }
    my $option_ns='';
    if (defined($options->{'ns'})) {
	$option_ns = ",".hash2options(
	    'ns' => $options->{'ns'},
	    );
    }
    my @accounts;
    for my $acc (sort keys %$pp) {
	my $t=$pp->{$acc};
	my $solde = Math::BigRat->new(0);
	my $init = 0;
	my $skip_amount = Math::BigRat->new(0);
	my $lines;
	my $options;
	my $limit_passed = (!defined($dlimit))|| $dlimit eq $dend;
	for my $p (sort 
		   {
		       $a->parent->var("rcdate_str") cmp $b->parent->var("rcdate_str")
			   or $a->var('keep') <=> $b->var('keep')
		   }		  
		   @$t) {
	    if ($p->var('keep') == 0) {
		$skip_amount += $p->var('amount');
		#print STDERR "Skipping ".val_str($p->var('amount'),2)." at date ", $p->parent->var("rcdate_str"), " for account ", $acc, "\n";
	    } else {
		if (!$init) {
		    $options .= hash2options(
			'account' => $acc,
			'datestart' => $dstart,
			'amountstart' => val_str($skip_amount,2),
			);
		    $init=1;
		    $solde = $skip_amount;
		    $skip_amount = Math::BigRat->new(0);
		}
		if ($skip_amount != 0) {
		    #$solde += $skip_amount;
		    if (not defined($dlimit) || ($dlimit gt $p->parent->var("rcdate_str"))) {
			warn "W: ", val_str($skip_amount,2),
			" € skipped for account ", $acc, " at ",
			$p->parent->var("rcdate_str"),"\n";
		    }
		}
		if (!$limit_passed && $dlimit lt $p->parent->var("rcdate_str")) {
		    $limit_passed=1;
		    $lines .= "  \\coproRCMarkLine{limit}{$dlimit}{".val_str($solde,2)."}%\n";
		}
		$solde += $p->var('amount');
		$lines .= "  \\coproRCDataLine{".
		    join('}{',
			 $p->parent->var("rcdate_str"),
			 $p->parent->date_str,
			 $p->parent->var("knowndate_str"),
			 latexize($p->parent->description),
			 $p->var('amount')->is_neg() ? "D" : "C",
			 val_str($p->var('amount')->is_neg() ? -$p->var('amount') : $p->var('amount'),2),
			 val_str($solde,2),
		    )."}%\n";
	    }
	}
	if ($init) {
	    $options .= ",".hash2options(
		'dateend' => $dend,
		'amountend' => val_str($solde,2),
		'amountendabs' => val_str($solde*($solde->is_neg()?-1:1),2),
		);
	    $str .= "\\coproRCDataAccount{$options$option_ns}{%\n$lines}%\n\n";
	    push @accounts, $acc;
	}
    }
    $str .= "\\coproRCDataAccountList{$option_ns}{%\n";
    for my $acc (@accounts) {
	$str .= "  {$acc},%\n";
    }
    $str .= "}\n\n";
    return $str;
}    
    
1;
