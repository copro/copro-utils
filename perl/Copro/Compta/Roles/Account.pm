package Copro::Compta::Roles::Account;
use Moose::Role;
use namespace::sweep;
use Copro::Compta::Account;
use Copro::Compta::Accounts;
use Carp;

sub accountname {
    my $self=shift;
    my $account=shift;
    if ($account eq '') {
	return $account;
    }
    if ($account !~ m/^([0-9:]+)(:([^0-9:].*))?$/) {
	#if ($account !~ m/^([0-9:]+)(:([A-Z][0-9A-Z:]*))?$/) {
	carp "Bad account $account";
	if ($account !~ m/^([0-9:]+)(:([^0-9:].*))?$/) {
	    die "Cannot not do anything with this account";
	}
    }
    my $base=$1;
    my $ext=$2 // '';
    $base =~ s/://g;
    $ext =~ s/:/-/g;
    #print "converting $account into $base$ext\n";
    return $base.$ext;
}

sub loadAccountsFromConfig {
    my $self=shift;
    my $config=shift;

    my $accounts=shift // Copro::Compta::Accounts->new();

    foreach my $ledger ($self->compta->select_ledgers($config,
						      'kind' => ['pc'])) {
	$accounts=$self->loadAccountsFromJournal($ledger->journal, $accounts);
    }

    return $accounts;
}

sub loadAccountsFromJournal {
    my $self=shift;
    my $journal=shift;
    my $accounts=shift // Copro::Compta::Accounts->new();

    my $iterator = $journal->iterator(
	'follow-includes'=>1,
	'select-element' => sub {
	    return shift->isa("Ledger::Account");
	},
	);
    
    while (my $account = $iterator->next) {
	my $laccount = $accounts->register(
	    $account->name_str,
	    );
	my $account_it = $account->iterator(
	    'select-element' => sub {
		my $e=shift;
		return $e->isa("Ledger::Account::Alias") ||
		    $e->isa("Ledger::Account::Note");
	    });
	while (my $account_elem = $account_it->next) {
	    if ($account_elem->isa("Ledger::Account::Alias")) {
		$laccount->addAlias($account_elem->name_str);
	    } else {
		$laccount->note($account_elem->note);
	    }
	}
	# ensure parents are loaded
	while (defined($laccount)) {
	    $laccount=$laccount->parent;
	}
    }

    return $accounts;
}

1;
