package Copro::Compta::Roles::ReadCopro;
use Moose::Role;
use namespace::sweep;

use Copro::Description;

has 'copro-description-dir' => (
    is      => 'rw',
    isa     => 'Str',
    accessor => 'descdir',
    default => 'archives/Copro',
    documentation => qq{Répertoire décrivant la copro},
    );

sub loadCoproDesc {
    my $self=shift;
    my $dir = shift;
    my $date = shift;
    my $desc={};

    if (ref($date) eq "") {
	$date = { 'default' => $date };
    }
    my $params = {
	'desc-dir' => $dir,
    };
    foreach my $d (keys %$date) {
	$params->{"$d-date"} = $date->{$d};
    }
    return Copro::Description->new(%$params);
}
    

1;
