package Copro::Compta::Roles::Appel;
use Moose::Role;
use namespace::sweep;

use utf8;
use Path::Class::File;
use Path::Class::Dir;

use Math::BigRat;
use Copro::Utils::Amount qw(amount_round amount_str);

use Data::Dumper;
sub appelCompute {
    my $self=shift;
    my %params=(@_);

    my $desc=$params{'copro'};
    my $amounts=$params{'amounts'};
    my $appel={};
    my $lots={};

    my $amount7=Math::BigRat->new(0);

    #print STDERR Dumper($amounts), "\n";
    foreach my $key (keys %{$amounts}) {
	my $keyinfo=$desc->info('keys', 'K.Id', $key);
	my $amount=amount_round($amounts->{$key});
	$amount7 += $amount;
	
	my $tantiemes=$desc->info('tantiemes', 'K.Id', $key);
	my $tot_tantiemes;
	my $next=$tantiemes->iterator;
	while (my $row = $next->()) {
	    $tot_tantiemes += Math::BigRat->new($row->{'tantiemes'});
	}
	$next=$tantiemes->iterator;
	while (my $row = $next->()) {
	    $lots->{$row->{'Lot'}} += 
		($amount * Math::BigRat->new($row->{'tantiemes'}))/$tot_tantiemes;
	}

	push @{$appel->{appels}}, {
	    'tot' => $tot_tantiemes,
	    'K.Id' => $key,
	    'K.Label' => $keyinfo->elm(0, 'K.Label'),
	    'amount' => $amount,
	};
    }
    #print STDERR Dumper($appel), "\n";
    my $next_cop=$desc->coprop->iterator;
    my $amount6;
    while (my $coprop = $next_cop->()) {
	my $id=$coprop->{'Coprop'};
	my $amount_copro;
	my $data={};
	foreach my $lot ($desc->info('lots', 'Coprop', $id)->col('Lot')) {
	    if (exists($lots->{$lot})) {
		my $value=amount_round($lots->{$lot});
		$data->{'lot'}->{$lot}=$value;
		$amount_copro += Math::BigRat->new($value);
	    }
	}
	if (defined($amount_copro)) {
	    $data->{'copro'}=$coprop;
	    $data->{'amount'}=$amount_copro;
	    $amount6 += $amount_copro;
	    push @{$appel->{'coprop'}}, $data;
	}
    }
    if (! ($amount7 - $amount6)->is_zero) {
	$appel->{'rompus'}=$amount7 - $amount6;
    }
    return $appel;
}

sub appel2string {
    my $self=shift;
    my $appel=shift;
    my $date=shift;

    my $res="$date\n";
    $res .= "    ; PHASE: REPARTITION\n";
    foreach my $part (sort {$a->{'K.Id'} cmp $b->{'K.Id'}} @{$appel->{appels}}) {
	$res .= "    ".sprintf("%-40s","701-CHCOUR")."  ".amount_str($part->{'amount'})."\n";
	$res .= "        ; ".$part->{'K.Label'}."\n";
	$res .= "        ; REPCLEF: ".$part->{'K.Id'}."\n";
	$res .= "        ; REPTYPECH: CHCOUR"."\n";
    }
    foreach my $part (@{$appel->{coprop}}) {
	$res .= "    ".sprintf("%-40s","450-".$part->{'copro'}->{'AccountSuffix'}).
	    "  ".amount_str(-$part->{'amount'})."\n";
	$res .= "        ; Copropriétaire : ".$part->{'copro'}->{'CopropTxtName'}."\n";
	foreach my $lot (sort { $a cmp $b } (keys %{$part->{'lot'}})) {
	    $res .= "        ; lot ".sprintf("%-4s",$lot." : ").amount_str(-$part->{'lot'}->{$lot})."\n";
	}
    }
    if (exists($appel->{'rompus'})) {
	my $account="472-ROMPUS";
	if ($appel->{'rompus'}<0) {
	    $account="471-ROMPUS";
	}
	$res .= "    ".sprintf("%-40s",$account)."  ".amount_str(-$appel->{'rompus'})."\n";
    }
    return $res;
}

sub appel2postings {
    my $self=shift;
    my $transaction=shift;
    my $appel=shift;

    foreach my $part (@{$appel->{coprop}}) {
	my $np=$transaction->addPosting(
	    'account' => "450-".$part->{'copro'}->{'AccountSuffix'},
	    'amount' => -$part->{'amount'},
	    );
	$np->addComment("Copropriétaire : ".$part->{'copro'}->{'CopropTxtName'});
	foreach my $lot (sort { $a cmp $b } (keys %{$part->{'lot'}})) {
	    $np->addComment("lot ".sprintf("%-4s",$lot." : ").amount_str(-$part->{'lot'}->{$lot}));
	}
    }
    if (exists($appel->{'rompus'})) {
	my $account="472-ROMPUS";
	if ($appel->{'rompus'}<0) {
	    $account="471-ROMPUS";
	}
	my $np=$transaction->addPosting(
	    'account' => $account,
	    'amount' => -$appel->{'rompus'},
	    );
    }
    return $transaction;
}

sub find_appels {
    my $self = shift;
    my $journal = shift;
    my $accounts = shift;
    my $appels={};
    
    my $iter_t=$journal->iterator(
	'follow-includes'=>1,
	'select-element' => sub {
            return shift->isa("Ledger::Transaction");
	});

    while (defined(my $t=$iter_t->next)) {
	if ($t->has_tag('APPEL')) {
	    if (!$t->has_tag('APPEL-DATE')) {
		die "Missing APPEL-DATE in $t\n";
	    }
	    push @{$appels->{$t->tag('APPEL')}}, $t;
	} elsif ($t->has_tag('NO-APPEL')) {
	    next;
	} else {
	    my $iter_p = $t->iterator(
		'select-element' => sub {
		    return shift->isa("Ledger::Posting");
		});
	    my $cpt7;
	    my $cpt450;
	    while (defined(my $p=$iter_p->next)) {
		my $account=$accounts->get($p->account->name);
		if (not defined($account)) {
		    die "Invalid account name ".$p->account->name;
		}
		my $n=$account->name;
		if ($n =~ /^7:/) {
		    $cpt7=1;
		} elsif ($n =~ /^4:5:0:/) {
		    $cpt450=1;
		}
	    }
	    if ($cpt7 && $cpt450) {
		warn "W: Missing APPEL tag for $t\n";
	    }
	}
    }
    return $appels;
}

1;
