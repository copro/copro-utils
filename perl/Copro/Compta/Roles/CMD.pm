package Copro::Compta::Roles::CMD;
use Moose::Role;
#use namespace::autoclean;

use Getopt::Long qw(:config require_order);
    
with 'Copro::Compta::Roles::ComptaAttr',
    'MooseX::Getopt' => { excludes => [qw(compta)] };
    #'MooseX::Getopt::Usage' => { excludes => [qw(compta)] },
    #'MooseX::Getopt::Usage::Role::Man';

requires qw( run synopsis );

sub cmdname {
    my $self = shift;
    my $name = $self->meta->name;
    $name =~ s/.*:://;
    return $name
}

sub help {
    my $self = shift;

    #foreach my $opts
}

#__PACKAGE__->meta->make_immutable;

1;
