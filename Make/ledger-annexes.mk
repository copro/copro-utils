
ifeq ($(COPRO_UTILS),)
$(warning COPRO_UTILS not defined)
endif

#include $(COPRO_UTILS)/Make/noalyss-download.mk

suffixes-by-year=balance.csv oprep.op.csv \
	$(foreach i,1 2 3 4 5,PCA$i.csv)
suffixes-by-year-n=$(suffixes-by-year) \
	op12.mop.csv \
	$(foreach i,1 2 3 4 5,PCA$i-all.csv) \

define noalyss-annexes
annexes-$1_DEPENDS+=annexes-$1-data.tex
annexes-$1-data.tex: annexes-$1.R $(COPRO_UTILS)/R/compta-annexes.R $(COPRO_UTILS)/R/annexes-common.R \
		$$(addprefix $$(DL)/pc,$$(addsuffix .csv,1 4 5 6 7)) \
		$$(addprefix $$(DL)/exercice-$1-,$$(call suffixes-by-year-n)) \
		$$(addprefix $$(DL)/exercice-$2-,$$(call suffixes-by-year)) \
		$$(wildcard budgets/budget-*-*.csv) \
		budgets/budget-$3.csv budgets/budget-$4.csv \
		$$(addprefix $$(DL)/exercice-$1-,balance-check-cloture.csv) \
		$$(foreach i,1 2 3 4 5,$$(DL)/pca-PCA$$i.csv) \
		$$(DL)/repartition.csv
	./annexes-$1.R

$$(DL)/exercice-$1-op12.mop.csv: $$(DL)/exercice-$2-op12.mop.csv

clean::
	$(RM) annexes-$1-data.tex
endef

download:
	$(MAKE) clean-download
	$(MAKE) login
	$(MAKE) $(foreach n,1 2 3 4 5 6 7 8 9,$(DL)/pc$n.csv)

.SECONDARY:
$(DL)/exercice-%-info.html: $(DL)/cookies.txt
	$(call WGET,do,ac=COMPTA/PRINT/PRINTBAL&exercice=$*)
	mv $@.html $@

define balance-select-ledgers
$$(DL)/exercice-%-balance$1.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt $$(DL)/ledgers.csv
	ledgers=$$$$(cat $$(DL)/ledgers.csv | $2 | cut -d \; -f 1 | sed -e 's/^/r_jrn[]=/') ; \
	JRN=$$$$(echo "$$$$ledgers" | tr '\n' '&')nb_jrn=$$$$(echo "$$$$ledgers" | wc -l) ; \
	FROM=$$$$(cat  $$< | awk -F ";" '{ if ($$$$4 == "$$*") { print $$$$1 } }' | head -n 1) && \
	TO=$$$$(cat  $$< | awk -F ";" '{ if ($$$$4 == "$$*") { print $$$$1 } }' | tail -n 1) && \
	$$(call WGET,export,ac=COMPTA/PRINT/PRINTBAL&exercice=$$*&from_periode='"$$$$FROM"'&to_periode='"$$$$TO"'&p_filter=1&'"$$$$JRN"'&from_poste=1&to_poste=8&act=CSV:balance&bt_csv=Export+CSV)
	$$(call cleanupCSV,$$@.html,$$@)
endef
$(eval $(call balance-select-ledgers,-check-cloture,$(SELECT_CLOTURE)))
$(eval $(call balance-select-ledgers,,$(SELECT_BALANCE)))
#$(eval $(call balance-select-ledgers,-avrep,grep -v 'Cloture exercise|Appels de fond.*répartition définitive'))

define transactions-by-ledger
$$(DL)/exercice-%$1.jrn.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt $$(DL)/ledgers.csv
	ledgers=$$$$(cat $$(DL)/ledgers.csv | $2 | cut -d \; -f 1 | sed -e 's/^/jrn_id=/') ; \
	if [ "$$$$ledgers" = "" ]; then echo "No ledger selected with '$2'. Aborting" ; exit 1 ; fi ; \
	JRN="$$$$ledgers" ; \
	if [ $$$$(echo "$$$$ledgers" | wc -l) != "1" ]; then echo "Bad number of ledger" ; exit 1 ; fi ; \
	FROM=$$$$(cat  $$< | awk -F ";" '{ if ($$$$4 == "$$*") { print $$$$1 } }' | head -n 1) && \
	TO=$$$$(cat  $$< | awk -F ";" '{ if ($$$$4 == "$$*") { print $$$$1 } }' | tail -n 1) && \
	$$(call WGET,export,act=CSV:ledger&ac=COMPTA/PRINT/PRINTJRN&type=jrn&'"$$$$JRN"'&from_periode='"$$$$FROM"'&to_periode='"$$$$TO"'&p_simple=0&bt_csv=Export+CSV)
	$$(call cleanupCSV,$$@.html,$$@)
endef
$(eval $(call transactions-by-ledger,-oprep,$(SELECT_REPART_DEFINITIVE)))
$(eval $(call transactions-by-ledger,-op12,$(SELECT_TROPEX_MULTIAN)))

$(DL)/exercice-%-regularisation.jrn.csv: $(DL)/periodes.csv $(DL)/cookies.txt
	FROM=$$(cat  $< | awk -F ";" '{ if ($$4 == "$*") { print $$1 } }' | head -n 1) && \
	TO=$$(cat  $< | awk -F ";" '{ if ($$4 == "$*") { print $$1 } }' | tail -n 1) && \
	$(call WGET,export,act=CSV:ledger&ac=COMPTA/PRINT/PRINTJRN&type=jrn&jrn_id=16&from_periode='"$$FROM"'&to_periode='"$$TO"'&p_simple=0&bt_csv=Export+CSV)
	$(call cleanupCSV,$@.html,$@)

