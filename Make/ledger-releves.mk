
ifeq ($(COPRO_UTILS),)
$(warning COPRO_UTILS not defined)
endif

#include $(COPRO_UTILS)/Make/noalyss-download.mk

define old-noalyss-get-releves
$$(DL)/releves-$1-gl.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt
	dstart="$$$$(cat $$(DL)/periodes.csv | grep ';$2;' | head -1 | cut -d \; -f 2)" && \
	$$(call WGET,export,bt_csv=Export+CSV&act=CSV:glcompte&type=poste&p_action=impress&from_periode='"$$$$dstart"'&to_periode=$3&from_poste=450&to_poste=451)
	mv $$@.html $$@
$$(DL)/releves-$1-ancgl.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt
	dstart="$$$$(cat $$(DL)/periodes.csv | grep ';$2;' | head -1 | cut -d \; -f 2)" && \
	$$(call WGET,export,bt_csv=Export+en+CSV&act=CSV:AncGrandLivre&from='"$$$$dstart"'&to=$3&pa_id=3&from_poste=&to_poste=)
	mv $$@.html $$@
endef

define noalyss-get-releves
$$(DL)/releves-$1.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt
	dstart="$$$$(cat $$(DL)/periodes.csv | grep ';$2;' | head -1 | cut -d \; -f 2)" && \
	$$(call WGET,export,bt_csv=Export+CSV&act=CSV:glcompte&type=poste&p_action=impress&from_periode='"$$$$dstart"'&to_periode=$3&from_poste=450&to_poste=451)
	echo "idaccount;labaccount;date;ref;lib;piece;let;deb;cred;solde;typesolde" > $$@
	awk < $$@.html -F \; '{ \
		if (NF==1) { \
			compte=$$$$1 \
		} else if ( $$$$1 == "Date" || $$$$1 == "" ) { toto=1 \
		} else { print (compte ";" $$$$0) } \
	}' | sed -e 's/^\([^ ]*\) - \([^;]*[^; ]\) *;/\1;\2;/' >> $$@
	$$(KEEP_TEMPS)$$(RM) $$@.html
$$(DL)/releves-$1-anc.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt $$(DL)/pca-infos.csv
	dstart="$$$$(cat $$(DL)/periodes.csv | grep ';$2;' | head -1 | cut -d \; -f 2)" && \
	PLAN=$$$$(cat $$(DL)/pca-infos.csv | grep "^$$(PCA1)" | cut -d \; -f 2) && \
	$$(call WGET,export,bt_csv=Export+en+CSV&act=CSV:AncGrandLivre&pa_id='"$$$$PLAN"'&from='"$$$$dstart"'&to=$3&from_poste=&to_poste=)
	$$(call cleanupCSV,$$@.html,$$@)
releves-$1-data.tex: releves-$1.R $(COPRO_UTILS)/R/compta-releves.R $(COPRO_UTILS)/R/annexes-common.R \
		$$(addprefix $$(DL)/releves-$1,.csv -anc.csv)
	./releves-$1.R
endef

define noalyss-releves
$$(eval $$(call noalyss-get-releves,$1,$2,$3,$4))

releves-$1-data.tex: $$(DL)/releves-$1.csv releves-$1.R
	./releves-$1.R $$< $$@ $3 $4

releves-$1_DEPENDS+=releves-$1-data.tex
endef

## https://compta.mens-vernet.fr/export.php?gDossier=25&bt_csv=Export+CSV&act=CSV%3Aglcompte&type=poste&p_action=impress&from_periode=09.04.2014&to_periode=01.02.2015&from_poste=450&to_poste=451
https://compta.mens-vernet.fr/export.php?act=CSV%3AAncGrandLivre&to=31.12.2014&from=04.01.2010&pa_id=3&from_poste=&to_poste=&gDossier=25&bt_csv=Export+en+CSV

