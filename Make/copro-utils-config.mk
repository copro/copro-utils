# Si COPRO_UTILS n'est pas encore définit, on le fait relativement
# au fichier courant
ifdef COPRO_UTILS
else
COPRO_UTILS:=$(abspath $(CURDIR)/$(dir $(lastword $(MAKEFILE_LIST)))..)
endif
export COPRO_UTILS

