
KEEP_TEMPS=
# for debug: avoid "rm" commands that remove intermediate files
#KEEP_TEMPS=echo  

# URL de noalyss
NOALYSS_URL=https://noalyss.exemple.com/
# Numéro du dossier à utiliser
NOALYSS_DOSSIER=12
# Login pour la connection (URL encoded if needed)
NOALYSS_USER=login
# Password pour la connection (URL encoded if needed)
NOALYSS_PASS=pass

# download directory
# This directory will be removed recursively on 'distclean' target
# do not put '.' in this variable or source files in the directory
DL=dl

# Si COPRO_UTILS n'est pas encore définit, on le fait relativement
# au fichier courant
ifdef COPRO_UTILS
else
COPRO_UTILS:=$(abspath $(CURDIR)/$(dir $(lastword $(MAKEFILE_LIST)))..)
endif
export COPRO_UTILS

# Regexp sur le fichier CSV de descriptions des PCA pour les 4 plans
# comptables analytiques utilisés
#
# PCA1 : clé à utilisé. Obligatoire sur toutes les transactions (6* 7*)
PCA1=PA1.*
# PCA2 : type de charges/recettes. Obligatoire sur toute les transactions (6* 7*)
# valeurs imposées : CHCOUR, CHTRAVAUX, CHEXCEPT
PCA2=PA2.*
# PCA3 : tropex (Travaux ou Opération Exceptionnelle)
# obligatoire chaque fois que PCA2 != CHCOUR, vide si PCA2 == CHCOUR
PCA3=PA3.*
# PCA4 : Ressources Affectées
# Pour les 7* : vide pour les appels de fonds, non-vide pour toutes les
#   autres ressources (intérêts, ...)
# Pour les 6* : non vide quand la dépense est "réglée" par l'utilisation
#   la ressources correspondante
PCA4=PA4.*
# PCA5 : Charges locatives
PCA5=PA5.*

# Sélection des journaux (appliqué au CSV de liste des journaux)
# On utilise des journaux particuliers, dont :
# * un pour les opérations "A Nouveau" en début d'exercice
# * un pour les opérations de cloture (inverse du précédent) en fin
#   d'exercice pour mettre à 0 tous les comptes 1* à 5*
# * un réservé pour les opérations (appels de fond) de répartition définitive
#   (et uniquement celle-ci)
# * un réservé pour les transfert sur (en fin d'exercice) et depuis (en
#   début d'exercice) le compte 12 pour les tropex multi-annuels non
#   finalisés en fin d'exercice
#
# Tout (y compris la "cloture")
# Util sur des périodes multi-exercice
SELECT_CLOTURE=cat
# Tout sauf les opérations de cloture
# Util sur un seul exercice, en particulier pour récupérer la balance correspondante
SELECT_BALANCE=grep -v 'Cloture exercise'
# Journal des opérations de répartition définitive
SELECT_REPART_DEFINITIVE=grep 'Appels de fond.*répartition définitive'
# Journal des opérations du compte 12
SELECT_TROPEX_MULTIAN=grep 'Transition exercice : attente travaux et op\. excep\.'
