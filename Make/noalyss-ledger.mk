
ifeq ($(COPRO_UTILS),)
$(warning COPRO_UTILS not defined)
endif

include $(COPRO_UTILS)/Make/noalyss-download.mk

define noalyss-get-exercice-gl
$$(DL)/exercice-$1-gl.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt
	dstart="$$$$(cat $$(DL)/periodes.csv | grep ';$1;' | head -1 | cut -d \; -f 2)" && \
	dend="$$$$(cat $$(DL)/periodes.csv | grep ';$1;' | tail -1 | cut -d \; -f 2)" && \
	$$(call WGET,export,bt_csv=Export+CSV&act=CSV:glcompte&type=poste&p_action=impress&from_periode='"$$$$dstart"'&to_periode='"$$$$dend"'&from_poste=&to_poste=)
	echo "idaccount;labaccount;date;ref;lib;piece;let;deb;cred;solde;typesolde;jcode;jname" > $$@
	awk < $$@.html -F \; '{ \
		if (NF==1) { \
			compte=$$$$1 \
		} else if ( $$$$1 == "Date" || $$$$1 == "" ) { toto=1 \
		} else { print (compte ";" $$$$0) } \
	}' | sed -e 's/^\([^ ]*\) - \([^;]*[^; ]\) *;/\1;\2;/' >> $$@
	$$(KEEP_TEMPS)$$(RM) $$@.html
$$(DL)/exercice-$1-gl-anc-%.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt $$(DL)/pca-infos.csv
	dstart="$$$$(cat $$(DL)/periodes.csv | grep ';$1;' | head -1 | cut -d \; -f 2)" && \
	dend="$$$$(cat $$(DL)/periodes.csv | grep ';$1;' | tail -1 | cut -d \; -f 2)" && \
	PLAN=$$$$(cat $$(DL)/pca-infos.csv | grep "^$$(PCA$$*)" | cut -d \; -f 2) && \
	$$(call WGET,export,bt_csv=Export+en+CSV&act=CSV:AncGrandLivre&pa_id='"$$$$PLAN"'&from='"$$$$dstart"'&to='"$$$$dend"'&from_poste=&to_poste=)
	$$(call cleanupCSV,$$@.html,$$@)

$$(DL)/exercice-$1: \
	$$(DL)/exercice-$1-gl.csv \
	$$(DL)/exercice-$1-gl-anc-1.csv \
	$$(DL)/exercice-$1-gl-anc-2.csv \
	$$(DL)/exercice-$1-gl-anc-3.csv \
	$$(DL)/exercice-$1-gl-anc-4.csv \
	$$(DL)/exercice-$1-gl-anc-5.csv
endef


## https://compta.mens-vernet.fr/export.php?gDossier=25&bt_csv=Export+CSV&act=CSV%3Aglcompte&type=poste&p_action=impress&from_periode=09.04.2014&to_periode=01.02.2015&from_poste=450&to_poste=451
##https://compta.mens-vernet.fr/export.php?act=CSV%3AAncGrandLivre&to=31.12.2014&from=04.01.2010&pa_id=3&from_poste=&to_poste=&gDossier=25&bt_csv=Export+en+CSV

