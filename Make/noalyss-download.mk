ifeq ($(_NOALYSS_MAKE_DOWNLOAD),)
_NOALYSS_MAKE_DOWNLOAD:=1

cleanupCSV=tr '\r' '\n' < "$1" > "$2" && $(RM) "$1"

WGET=wget --load-cookies=$(DL)/cookies.txt.session --output-document=$@.html \
	--save-cookies=$(DL)/cookies.txt.session --keep-session-cookies $3 \
	'$(NOALYSS_URL)$1.php?gDossier=$(NOALYSS_DOSSIER)&$2' && \
	if grep -sq "Utilisateur déconnecté" $@.html ; then \
		$(RM) -v $(DL)/cookies.txt $(DL)/cookies.txt.session $@.html ; \
		exit 1 ;\
	fi

login: $(DL)/cookies.txt

$(DL)/stamp:
	mkdir -p $(dir $@)
	touch $@

$(DL)/cookies.txt: $(DL)/stamp $(COPRO_UTILS)/Make/noalyss-annexes.mk
	echo building $@ due to $?
	echo '$(subst ','"'"',p_user=$(NOALYSS_USER)&p_pass=$(NOALYSS_PASS)&login=Se+connecter)' > $(DL)/credential.txt
	wget --output-document=$@.html --save-cookies=$@ --keep-session-cookies \
		--post-file=$(DL)/credential.txt \
		'$(NOALYSS_URL)login.php' || $(RM) $@ $(DL)/credential.txt
	$(RM) $(DL)/credential.txt
	cp $@ $@.session
	touch $@
	$(KEEP_TEMPS)$(RM) $@.html

$(DL)/pc%.csv: $(DL)/cookies.txt
	$(call WGET,export,act=METADATA&type=pc&p_start=$*&p_end=$*)
	mv $@.html $@

%.op.csv: %.jrn.csv Makefile
	echo '"Num";"Pièce";"Num.interne";"Date";"what?";"libelle";"deb";"cred"' > "$@"
	grep -v '^"";' "$<" >> "$@" || true

%.mop.csv: %.jrn.csv Makefile
	echo '"Num";"Pièce";"Num.interne";"Date";"what?";"libelle";"deb";"cred"' > "$@"
	grep -v '^"";' "$<" >> "$@" || true
	grep -v '^"Num";"Pièce";"Num.interne"' $(filter %.mop.csv,$^) >> "$@" || true

$(DL)/ledgers.csv: $(DL)/cookies.txt
	$(call WGET,export,act=METADATA&type=ledger)
	mv $@.html $@

$(DL)/repartition.csv: $(DL)/cookies.txt
	$(call WGET,export,act=METADATA&type=repartition)
	mv $@.html $@

$(DL)/periodes.csv: $(DL)/cookies.txt
	$(call WGET,export,act=METADATA&type=periode)
	mv $@.html $@

$(DL)/pca-infos.csv: $(DL)/cookies.txt
	$(call WGET,export,act=METADATA&type=pa)
	mv $@.html $@

define PCA-select
$$(DL)/exercice-%-$1$2.csv: $$(DL)/periodes.csv $$(DL)/cookies.txt $$(DL)/pca-infos.csv
	$$(call WGET,do,ac=ANC/ANCIMP/ANCGL)
	PLAN=$$$$(cat $$(DL)/pca-infos.csv | grep "^$$($1)" | cut -d \; -f 2) && \
	FROM=$$(if $2,,$$$$(cat  $$< | awk -F ";" '{ if ($$$$4 == "$$*") { print $$$$2 } }' | head -n 1 )) && \
	TO=$$$$(cat  $$< | awk -F ";" '{ if ($$$$4 == "$$*") { print $$$$3 } }' | tail -n 1) && \
	$$(call WGET,export,act=CSV:AncGrandLivre&from='"$$$$FROM"'&to='"$$$$TO"'&pa_id='"$$$$PLAN"'&from_poste=&to_poste=&bt_csv=Export+en+CSV)
	$$(call cleanupCSV,$$@.html,$$@)
endef
$(eval $(call PCA-select,PCA1))
$(eval $(call PCA-select,PCA2))
$(eval $(call PCA-select,PCA3))
$(eval $(call PCA-select,PCA4))
$(eval $(call PCA-select,PCA5))
$(eval $(call PCA-select,PCA1,-all))
$(eval $(call PCA-select,PCA2,-all))
$(eval $(call PCA-select,PCA3,-all))
$(eval $(call PCA-select,PCA4,-all))
$(eval $(call PCA-select,PCA5,-all))

define PCA-labels
$$(DL)/pca-$1.csv: $$(DL)/pca-infos.csv $$(DL)/cookies.txt
	PLAN=$$$$(cat $$(DL)/pca-infos.csv | grep "^$$($1)" | cut -d \; -f 2) && \
	$$(call WGET,export,act=METADATA&type=po&pa_id='"$$$$PLAN"')
	mv $$@.html $$@
endef
$(eval $(call PCA-labels,PCA1,clef))
$(eval $(call PCA-labels,PCA2,type))
$(eval $(call PCA-labels,PCA3,tropex))
$(eval $(call PCA-labels,PCA4,RA))
$(eval $(call PCA-labels,PCA5,chloc))

clean-download::
	$(RM) -r $(DL)

endif
