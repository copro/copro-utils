
ifeq ($(COPRO_UTILS),)
$(error COPRO_UTILS not defined!)
endif

include $(COPRO_UTILS)/Make/LaTeX-support.mk

AG_DATE=$(notdir $(CURDIR))
AG_YEAR=$(firstword $(subst -, ,$(AG_DATE)))
AG_DOC_PREFIX ?= AG-$(AG_DATE)-

AG_DOC_PV=$(AG_DOC_PREFIX)PV
AG_DOC_PVS=$(AG_DOC_PREFIX)PV-signe
AG_DOC_CONVOC=$(AG_DOC_PREFIX)convoc
AG_DOC_AR_CONVOC=$(AG_DOC_PREFIX)convoc-AR
AG_DOC_POUVOIRS=$(AG_DOC_PREFIX)pouvoirs

LU_MASTERS=$(AG_DOC_CONVOC) \
	$(AG_DOC_PV) $(AG_DOC_AR_CONVOC) $(AG_DOC_POUVOIRS) \
	$(AG_DOC_PVS) \
 $(filter-out convocation cr convoc-AR pouvoirs,$(_LU_MASTERS_MK))

$(AG_DOC_CONVOC)_DEPENDS += $(convocation_DEPENDS)
$(AG_DOC_PVS)_DEPENDS += \
        scans/signatures-CR.pdf \
        scans/fiche-presents.pdf \
        $(cr_DEPENDS)
$(AG_DOC_PV)_DEPENDS += \
        $(cr_DEPENDS)

$(AG_DOC_PVS)_MAIN = cr.tex
$(AG_DOC_PV)_MAIN = cr.tex
$(AG_DOC_CONVOC)_MAIN = convocation.tex
$(AG_DOC_AR_CONVOC)_MAIN = convoc-AR.tex
$(AG_DOC_POUVOIRS)_MAIN = pouvoirs.tex


AG_DOCS_CONVOC += convocation-provisoire.pdf \
	$(AG_DOC_CONVOC).pdf \
	recepisse-convocation.pdf pouvoir.pdf
AG_DOCS_PRE_AG += docs.pdf $(AG_DOC_AR_CONVOC).pdf
AG_DOCS_PRE_PV += $(AG_DOC_POUVOIRS).pdf $(AG_DOC_PV).pdf
AG_DOCS_PV += $(AG_DOC_PVS).pdf

AG_DOCS_TO_ARCHIVE += $(AG_DOC_CONVOC).pdf \
	$(AG_DOC_AR_CONVOC).pdf $(AG_DOC_POUVOIRS).pdf \
	$(AG_DOC_PV).pdf $(AG_DOC_PVS).pdf

.PHONY: ag-docs-convoc ag-docs-preparation-pv ag-docs-pv ag-docs-all
ag-docs-convoc:: $(AG_DOCS_CONVOC)
	@echo "Documents préparés : $^"
ag-docs-preparation-ag:: $(AG_DOCS_PRE_AG)
	@echo "Documents préparés : $^"
ag-docs-preparation-pv:: $(AG_DOCS_PRE_PV)
	@echo "Documents préparés : $^"
ag-docs-pv:: $(AG_DOCS_PV)
	@echo "Documents préparés : $^"
ag-docs-all:: ag-docs-convoc ag-docs-preparation-ag \
	ag-docs-preparation-pv ag-docs-pv

%-provisoire.pdf: %.pdf $(COPRO_UTILS)/stamp.pdf
	pdftk $*.pdf stamp $(COPRO_UTILS)/stamp.pdf output $@

convocation-provisoire.pdf: $(AG_DOC_CONVOC).pdf $(COPRO_UTILS)/stamp.pdf
	pdftk $< stamp $(COPRO_UTILS)/stamp.pdf output $@

PV-provisoire.pdf: $(AG_DOC_PV).pdf $(COPRO_UTILS)/stamp.pdf
	pdftk $< stamp $(COPRO_UTILS)/stamp.pdf output $@

pouvoir.pdf: $(AG_DOC_CONVOC).pdf
	pdftk $< cat end output $@

distclean::
	$(RM) pouvoir.pdf

final/%: %
	@if test -e $@; then \
		if cmp -s $< $@; then \
			echo "ARCHIVE: skipping $< (already archived)"; \
			touch "$@"; \
		else \
			echo "ARCHIVE: skipping $< (already archived BUT different)"; \
		fi ;\
	else \
		cp -av $< $@ ; \
	fi

do-archive: $(addprefix final/,$(AG_DOCS_TO_ARCHIVE))
	$(MAKE) archive

.PHONY:
AG_ARCHIVE_DIR=../../archives/Exercices/$(AG_YEAR)/AGs/$(AG_DATE)
archive:
	mkdir -p $(AG_ARCHIVE_DIR)
	rsync -av final/. $(AG_ARCHIVE_DIR)/.
	rsync -avn --delete final/. $(AG_ARCHIVE_DIR)/.
