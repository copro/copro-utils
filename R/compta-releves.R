
if (Sys.getenv("COPRO_UTILS")!="") {
    source(file.path(Sys.getenv("COPRO_UTILS"), "R", "compta-common.R"))
} else {
    source(file.path("compta-common.R"))
}   

date.ex.init <- function() {
    df=data.frame(ref=c(NA), date.ex=c(NA))
    df[FALSE,]
}
date.ex.add <- function(df, ref, date) {
    rbind(df, data.frame(ref=ref, date.ex=as.Date(date)))
}
date.ex.apply <- function(df, de) {
    df=merge(x=df, y=de, by=c("ref"), suffixes=c("", ".fixed"), all.x=TRUE)
    df$date.ex[!is.na(df$date.ex.fixed)]=df$date.ex.fixed[!is.na(df$date.ex.fixed)]
    df$date.ex.fixed=NULL
    df
}

cle.label.init <- function() {
    df=data.frame(cle=c(NA), clabel=c(NA))
    df[FALSE,]
}
cle.label.add <- function(df, cle, lab) {
    rbind(df, data.frame(cle=cle, clabel=lab))
}
cle.label.apply <- function(df, dcl) {
    df=merge(x=df, y=dcl, by=c("cle"), all.x=TRUE)
    missing=df[ is.na(df$clabel), "cle"]
    if (length(missing)>0) {
        compta.warning("Des clés sont manquantes", unique(missing))
        df$clabel <- as.character(df$clabel)
        df$clabel[is.na(df$clabel)] <- as.character(df$cle[is.na(df$clabel)])
    }
    df$cle=df$clabel
    df$clabel=NULL
    df
}
   

read.gl <- function(file, fix.date.ex=date.ex.init(), date.init=NULL) {
    data <- read.csv2(file)
    data$idaccount=as.factor(data$idaccount)
    data$orig=as.factor("File")
    data$soo=1
    data$so=row.names(data)
    data$solde=NULL
    data$typesolde=NULL
    data$montant=compute.val.by.diff(data, na.rm=TRUE)
    data$deb=NULL
    data$cred=NULL
    data$date.compta=as.Date(fix.date(data$date))
    data$date=NULL
    ag=grepl('^AG:20[01][0-9]-[01][0-9]-[0-3][0-9]:[0-9]*$', data$piece)
    data$date.ag=as.Date(ifelse(ag,sub('AG:([0-9-]*):.*', '\\1', data$piece),NA))
    if (!is.null(date.init)) {
        date.init=as.Date(date.init)
        data=rbind.fill(
            data[data$date.compta >= date.init,],
            ddply(data, .(idaccount, labaccount), function(df) {
                data.frame(soo=0, orig="Init", date.compta=date.init,
                           montant=sum(df$montant[df$date.compta<date.init
                               | (df$date.compta==date.init & grepl('^ANOUV20[01][0-9]$', df$piece))]))
            }))
        ##print(list(data=data))
    }
    data=data[!grepl('^(CLO|ANOUV)20[01][0-9]$', data$piece),]
    data=ddply(data, .(idaccount), transform, solde=round(cumsum(montant),4))
    data$date.ex=data$date.compta
    data=date.ex.apply(data, fix.date.ex)
    data
}

read.glanc <- function(file, fix.date.ex=date.ex.init(), postes.regex='^70') {
    data <- read.csv2(file)
    data$orig=as.factor("File")
    data$so=row.names(data)
    data$soo=1
    data$cle=data$"Imp..Analytique"
    data$"Imp..Analytique"=NULL
    data$date.compta=as.Date(fix.date(data$Date))
    data$Date=NULL
    data$ref=data$Num.interne
    data$Num.interne=NULL
    data=data[grepl(postes.regex, data$Poste),]
    data$Quick_Code=NULL
    data$piece=data$Pièce
    data$Pièce=NULL
    data$montant=compute.val.by.type(data)
    data$Debit=NULL
    data$Credit=NULL
    data$date.ex=data$date.compta
    ##print(list(data=data))
    data=date.ex.apply(data, fix.date.ex)
    data
}

releve.compte=function(data, account, dates.solde=c(), date.clo=NULL) {
    data.account=data[data$idaccount == account, ]
    labaccount=unique(data.account$labaccount)
    if (length(dates.solde)>0) {
        data.account=rbind.fill(
            data.account,
            data.frame(idaccount=account, labaccount=labaccount,
                       soo=2,
                       orig="Solde", date.compta=as.Date(dates.solde),
                       date.ex=as.Date(dates.solde)))
    }
    if (!is.null(date.clo)) {
        data.account=data.account[data.account$date.compta <= date.clo,]
        data.account=rbind.fill(
            data.account,
            data.frame(idaccount=account, labaccount=labaccount,
                       soo=3,
                       orig="Clo", date.compta=as.Date(date.clo),
                       date.ex=as.Date(date.clo)))
    }
    data.account=arrange(data.account, date.compta, soo, so)
    montant=ifelse(is.na(data.account$montant), 0, data.account$montant)
    data.account$solde=round(cumsum(montant),4)
    data.account$piece=NULL
    #data.account$ref=NULL
    data.account
}

print.releve.compte <- function(rel, file=NULL, cmdprefix='releve') {
    cat(file=file, sep="",
        "\\expandafter\\newcommand\\csname ", cmdprefix,
        as.character(unique(rel$idaccount)), "\\endcsname{%\n")

    ##"  \\annE", ltype, "{",
    ##    paste.param(i, type, tropex, clef, libelle), "}{%\n"
    
    for(i in seq_len(nrow(rel))) {
        l=rel[i,]
        #print(list(l=l))
        cat(file=file, sep="",
            "  \\relaccount", as.character(l$orig),
            "{", ifelse(l$montant<0, -l$montant, "0"),
            "}{", ifelse(l$montant>0, l$montant, ""),
            "}{", l$solde,
            "}{", as.character(l$date.compta), "}{", as.character(l$lib), "}\n")
    }
    cat(file=file, sep="", "}\n")
    cat(file=file, sep="",
        "\\expandafter\\newcommand\\csname ", cmdprefix,
        as.character(unique(rel$idaccount)), "Solde\\endcsname{%\n")
    cat(file=file, sep="", tail(rel$solde, n=1), "}\n\n")
}

print.releve.compte.all <- function(gl, file=NULL, cmdprefix='releve', dates.solde=c(), date.clo=NULL) {
   ddply(gl, .(idaccount), function(df) {
        print.releve.compte(
            releve.compte(df, unique(df$idaccount), dates.solde=dates.solde, date.clo=date.clo),
            file=file, cmdprefix=cmdprefix)
    })
    1
}

appels.fonds <- function(gl, glanc, account, date.init=NULL, date.clo=NULL) {
    if (!is.null(date.init)) {
        glanc=glanc[glanc$date.compta >= date.init,]
    }
    if (!is.null(date.clo)) {
        glanc=glanc[glanc$date.compta <= date.clo,]
    }
    data=merge(glanc, gl[c("ref", "idaccount", "labaccount", "montant", "date.ag")],
        by="ref", suffixes=c(".anc", ".glob"))
    data=arrange(data, idaccount, date.ex, cle)
    data
}

print.appels.fonds <- function(af, file=NULL, cmdprefix='appfonds', cle.label=cle.label.init()) {
    af=cle.label.apply(af, cle.label)
    ddply(af, .(idaccount), function(df) {
        idaccount=as.character(unique(df$idaccount))
        cat(file=file, sep="",
            "\\expandafter\\newcommand\\csname ", cmdprefix,
            idaccount, "\\endcsname{%\n")
        df$so=as.numeric(df$so)
        df=arrange(df, date.ex, so)
        ##print(list(df=df))
        for (ref in unique(df$ref)) {
            op=df[df$ref==ref,]
            if (nrow(op) == 1) {
                cat(file=file, sep="",
                    "  \\appFondsOneKey{",
                    paste(sep="}{",
                          idaccount, as.character(op$date.compta),
                          as.character(op$date.ag), as.character(op$date.ex),
                          as.character(op$libelle), -op$montant.glob,
                          op$montant.anc, as.character(op$cle)),
                    "}%\n")
            } else {
                opc=unique(op[,c("montant.glob", "date.compta", "date.ag",
                    "date.ex", "libelle", "montant.glob")])
                if (nrow(opc)!=1) {
                    compta.warning("Strange data not unique:", opc)
                    opc=opc[1,]
                }
                nop=nrow(op)
                cat(file=file, sep="",
                    "  \\appFondsMultiKeys{",
                    paste(sep="}{",
                          idaccount, as.character(opc$date.compta),
                          as.character(opc$date.ag), as.character(opc$date.ex),
                          as.character(opc$libelle), -opc$montant.glob,
                          sum(op$montant.anc), nop+1),
                    "}{%\n")
                for(i in seq_len(nop)) {
                    opu=op[i,]
                    cat(file=file, sep="",
                        "    \\appFondsKey{",
                        paste(sep="}{",
                              nop, i, opu$montant.anc, as.character(opu$cle)),
                        "}%\n")
                }
                cat(file=file, sep="", "  }%\n")
            }
        }
        df.glob=ddply(df, .(ref), summarize, montant.glob=unique(montant.glob))
        cat(file=file, sep="",
            "  \\appFondsTotal{",
            paste(sep="}{",
                  idaccount, -sum(df.glob$montant.glob), sum(df$montant.anc)),
            "}\n")
        cat(file=file, sep="", "}\n")
    })
    1
}



