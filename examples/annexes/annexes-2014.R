#!/usr/bin/Rscript

show.details=FALSE
##show.details=TRUE

source(file.path(Sys.getenv("COPRO_UTILS"), "R", "compta-annexes.R"))

name.nm1 = 2009
name.n = 2014
name.np1 = name.n+1
name.np2 = name.np1+1

tropex.names.n=c("2012-EDD", "2014-provisions-rr",
    "2014-avances-tresorerie", "2014-toitBatA", "2014-DTA-CREP")

tropex.report=c(
    "20120626EDD",
    "20140709TOITBATA"
)

coproprietaires.set=function(annexe) {
    annexe.coproprietaires.set(annexe, copro.max=10, copro.done='450008')
}

source(file.path(Sys.getenv("COPRO_UTILS"), "R", "annexes-common.R"))
