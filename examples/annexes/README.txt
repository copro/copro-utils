Exemple pour obtenir des annexes comptables avec noalyss, R et LaTeX.

Cet exemple provient de ma copropriété. Les budgets (annuels et pour chaque
opération exceptionnelle/travaux) doivent bien sûr être adaptés.
  La copro a eu une situation un peu particulière (pas d'AG ni de
régularisation de comptes de juin 2009 à avril 2014, d'où des dates d'exercice
très étranges pour cet exemple)

4 plans comptables analytiques sont nécessaires pour extraire automatiquement
(ou presque) ces annexes ainsi que quelques journaux spécifiques. Voir
../../Make/noalyss-config.mk pour plus d'infos.

Patchs (y compris documentation) bienvenus pour améliorer tout cela. Je n'ai
pas le temps d'assurer du support, mais des personnes peuvent quand même être
intéressées...


config.mk.template doit être renommé en config.mk (et complété)
../../Make/noalyss-config.mk devra probablement aussi être adapté
(nom des PCA et des journaux spéciaux, ...)
